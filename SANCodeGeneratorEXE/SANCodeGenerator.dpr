program SANCodeGenerator;

uses
  SysUtils,
  Forms,
  CGDM_u;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.HintHidePause := 30000;

  CreateSANCodeGeneratorAsApp;

  Application.Run;
end.
