object frameConditionParamsDefs: TframeConditionParamsDefs
  Left = 0
  Top = 0
  Width = 638
  Height = 376
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object pnlClient: TPanel
    Left = 0
    Top = 0
    Width = 638
    Height = 376
    Align = alClient
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    object spltrVrt: TSplitter
      Left = 185
      Top = 0
      Width = 6
      Height = 376
      Color = clBtnFace
      MinSize = 1
      ParentColor = False
      ExplicitLeft = 194
      ExplicitTop = 1
      ExplicitHeight = 374
    end
    object pnlCndParams: TPanel
      Left = 0
      Top = 0
      Width = 185
      Height = 376
      Align = alLeft
      ShowCaption = False
      TabOrder = 0
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitHeight = 372
      object lblCndParam: TLabel
        Left = 4
        Top = 0
        Width = 118
        Height = 13
        Caption = #1059#1089#1083#1086#1074#1085#1099#1081' '#1087#1072#1088#1072#1084#1077#1090#1088
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbxCndParams: TListBox
        AlignWithMargins = True
        Left = 5
        Top = 17
        Width = 175
        Height = 354
        Margins.Left = 4
        Margins.Top = 16
        Margins.Right = 4
        Margins.Bottom = 4
        Style = lbVirtual
        Align = alClient
        TabOrder = 0
        OnClick = lbxCndParamsClick
        OnData = lbxCndParamsData
      end
    end
    object pnlDefaults: TPanel
      Left = 191
      Top = 0
      Width = 447
      Height = 376
      Align = alClient
      ShowCaption = False
      TabOrder = 1
      ExplicitLeft = 193
      ExplicitTop = 2
      ExplicitWidth = 443
      ExplicitHeight = 372
      object lblDefaults: TLabel
        Left = 5
        Top = 0
        Width = 153
        Height = 13
        Caption = #1053#1072#1073#1086#1088' '#1087#1088#1077#1076#1079#1072#1076#1072#1085#1099#1093' '#1079#1085#1072#1095#1077#1085#1080#1081
      end
      object mmDefaults: TMemo
        AlignWithMargins = True
        Left = 5
        Top = 17
        Width = 437
        Height = 354
        Margins.Left = 4
        Margins.Top = 16
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        OnChange = mmDefaultsChange
        ExplicitWidth = 433
        ExplicitHeight = 350
      end
    end
  end
end
