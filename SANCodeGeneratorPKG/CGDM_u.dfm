object CGDM: TCGDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 150
  Width = 311
  object dlgOpenTemplate: TOpenDialog
    DefaultExt = '.txt'
    Filter = #1058#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083'(*.txt)|*.txt|'#1051#1102#1073#1086#1081' '#1092#1072#1081#1083'(*.*)|*.*'
    Options = [ofHideReadOnly, ofNoChangeDir, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
    Left = 32
    Top = 8
  end
  object dlgSaveTemplate: TSaveDialog
    DefaultExt = '.txt'
    Filter = #1058#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083'(*.txt)|*.txt|'#1051#1102#1073#1086#1081' '#1092#1072#1081#1083'(*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofNoChangeDir, ofPathMustExist, ofEnableSizing]
    Left = 128
    Top = 8
  end
  object xmlConfig: TXMLDocument
    Options = [doNodeAutoCreate, doNodeAutoIndent, doAttrNull, doAutoPrefix, doNamespaceDecl]
    Left = 216
    Top = 8
    DOMVendorDesc = 'MSXML'
  end
end
