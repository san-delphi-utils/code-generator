UNIT frameScriptSign_u;/////////////////////////////////////////////////////////
// Фрейм настройки элемента скрипта
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CGDM_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons;
//==============================================================================
TYPE
  TColorSpeedButton = class(TSpeedButton)
  protected
    procedure Paint; override;
  end;//TColorSpeedButton
//==============================================================================
  TframeScriptSign = class(TFrame)
    lblSignBeg: TLabel;
    edtSignBeg: TEdit;
    gbScriptSign: TGroupBox;
    edtColor: TEdit;
    lblColor: TLabel;
    dlgColor: TColorDialog;
    lblSignEnd: TLabel;
    edtSignEnd: TEdit;
    procedure edtSignBegChange(Sender: TObject);
    procedure edtSignEndChange(Sender: TObject);
    procedure edtColorChange(Sender: TObject);
    procedure edtColorKeyPress(Sender: TObject; var Key: Char);
    procedure edtColorMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);

  private
    FSign : TScriptSign;

    FSbtnColor : TSpeedButton;

  protected
    procedure SignChanged(Sender : TScriptSign; const AUpdatedFilelds : TScriptSignFileds);
    procedure SbtnColorClick(Sender: TObject);

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function IsValid : boolean;

  public
    property Sign : TScriptSign  read FSign;
  end;//TframeScriptSign
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
// TColorSpeedButton
//==============================================================================
procedure TColorSpeedButton.Paint;
const
  RECT_INDENT = 4;
var
  vColor : TColor;
begin
  inherited;

  vColor := (Owner.Owner as TframeScriptSign).Sign.Color;

  with Canvas do  begin
    Pen.Color := clBlack;
    Brush.Color := vColor;
    Rectangle(RECT_INDENT, RECT_INDENT, ClientWidth - RECT_INDENT, ClientHeight - RECT_INDENT);
  end;//with
end;//TColorSpeedButton.Paint
//==============================================================================


//==============================================================================
// TframeScriptSign
//==============================================================================
// protected
//==============================================================================
procedure TframeScriptSign.SignChanged(Sender: TScriptSign;
  const AUpdatedFilelds: TScriptSignFileds);
begin

  if ssfSignBeg in AUpdatedFilelds then
    edtSignBeg.Text := Sender.SignBeg;

  if ssfSignEnd in AUpdatedFilelds then
    edtSignEnd.Text := Sender.SignEnd;

  if ssfColor in AUpdatedFilelds then  begin
    edtColor.Text := IntToHex(Sender.Color, 6);

//    with FSbtnColor.Glyph.Canvas do  begin
//      Pen.Color := clBlack;
//      Brush.Color := Sender.Color;
//      FillRect(ClipRect);
////      Rectangle(0, 0, ClientWidth, ClientHeight);
//    end;//with


    edtColor.Repaint;
  end;//if
//    clrbxColor.Selected := Sender.Color;
end;//TframeScriptSign.SignChanged
//==============================================================================
procedure TframeScriptSign.SbtnColorClick(Sender: TObject);
begin
  with dlgColor do  begin
    Color := Sign.Color;

    if Execute then
      Sign.Color := Color;
  end;//with
end;//TframeScriptSign.SbtnColorClick
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeScriptSign.Create(AOwner: TComponent);
begin
  inherited;

  FSign := TScriptSign.Create(nil, 'frameScriptSign', False, 0);
  FSign.OnChange := SignChanged;

  FSbtnColor := TColorSpeedButton.Create(edtColor);
  with FSbtnColor do  begin
    Height := edtColor.ClientHeight - 2;
    Width := Height;
    Top := 0;
    Left := edtColor.ClientWidth - Width - 3;
    Cursor := crArrow;
    OnClick := SbtnColorClick;
    Parent := edtColor;
  end;//with
end;//TframeScriptSign.Create
//==============================================================================
destructor TframeScriptSign.Destroy;
begin
  FreeAndNil(FSign);

  inherited;
end;//TframeScriptSign.Destroy
//==============================================================================
function TframeScriptSign.IsValid: boolean;
// True, если нормальные значения параметров
begin
  Result := (edtSignBeg.Text <> '') and ((not Sign.IsRanged) or (edtSignEnd.Text <> ''))
end;//TframeScriptSign.IsValid
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeScriptSign.edtSignBegChange(Sender: TObject);
begin
  FSign.SignBeg := (Sender as TEdit).Text;
end;//TframeScriptSign.edtSignChange
//==============================================================================
procedure TframeScriptSign.edtSignEndChange(Sender: TObject);
begin
  FSign.SignEnd := (Sender as TEdit).Text;
end;//TframeScriptSign.edtSignEndChange
//==============================================================================
procedure TframeScriptSign.edtColorChange(Sender: TObject);
begin
  FSign.Color := StringToColor('$' + (Sender as TEdit).Text);
  FSbtnColor.Repaint;
end;//TframeScriptSign.edtColorChange
//==============================================================================
procedure TframeScriptSign.edtColorKeyPress(Sender: TObject; var Key: Char);
begin
  if not(CharInSet(Key, ['0'..'9', 'A'..'F'])) then  begin
    MessageBeep(MB_OK);
    Key := #0;
  end;//if
end;//TframeScriptSign.edtColorKeyPress
//==============================================================================
procedure TframeScriptSign.edtColorMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  FSbtnColor.Repaint;
end;//TframeScriptSign.edtColorMouseMove
//==============================================================================


END.

