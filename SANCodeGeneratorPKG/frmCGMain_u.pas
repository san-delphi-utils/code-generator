UNIT frmCGMain_u;///////////////////////////////////////////////////////////////
// Главная форма генератора кода
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CGDM_u,
  frameTemplateCode_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, Types, StrUtils, Math, TypInfo,
  AppEvnts, ActnList, Grids, ImgList, Menus, frameCustomDrawGrid_u,
  frameCyclicParams_u, frameConditionParamsDefs_u, frameConstants_u;
//==============================================================================
CONST
  CLOSE_BUTTON_RIGHT_INDENT  = 11;
  SPACES_IN_TAB              = 7;
  SG_PARAMS_FIRST_ROW_COUNT  = 1;
  SG_PARAMS_FIRST_COL_COUNT  = 2;
//==============================================================================
TYPE
  {$I TabControl_CloseButtons_Intf.inc}
//==============================================================================
  TSplitter = class(ExtCtrls.TSplitter)
  private
    FParentHeight, FParentClientHeight, FBottomControlHeigth : integer;

    FsbtnUp, FsbtnMiddle, FsbtnDown : TSpeedButton;

    FTopControl, FBottomControl : TControl;

  private  type
    TSplitterButton = (sbUp, sbMiddle, sbDown);

  private
    function Get_SpeedButton(AButton: TSplitterButton): TSpeedButton;

  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;

    function BottonsExists : boolean;

    procedure SetParent(AParent: TWinControl); override;

    procedure Resize; override;

    procedure Loaded; override;

    procedure BottonClick(Sender : TObject);

  public
    constructor Create(AOwner : TComponent); override;

  public
    property Buttons[AButton : TSplitterButton] : TSpeedButton  read Get_SpeedButton;

  end;//TSplitter
//==============================================================================
  TfrmCGMain = class(TForm)
    pnlClient: TPanel;
    pnlButtons: TPanel;
    pnlParams: TPanel;
    pnlCodes: TPanel;
    tcCodes: TTabControl;
    spltrCodeAndParams: TSplitter;
    btnGenerate: TBitBtn;
    btnInsert: TBitBtn;
    btnCopyToClpBrd: TBitBtn;
    btnClose: TBitBtn;
    sbtnTemplateNew: TSpeedButton;
    sbtnTemplateOpen: TSpeedButton;
    sbtnTemplateCloseAll: TSpeedButton;
    sbtnTemplateCloseAllExceptThis: TSpeedButton;
    pnlCodesTabsBottons: TPanel;
    sbtnTemplateSave: TSpeedButton;
    sbtnTemplateRemember: TSpeedButton;
    ae: TApplicationEvents;
    actList: TActionList;
    actTemplateSave: TAction;
    btnTools: TBitBtn;
    sbtnToggleFormState: TSpeedButton;
    btnReCreate: TBitBtn;
    pcParams: TPageControl;
    pnlParamsBottons: TPanel;
    tsParamsTextSource: TTabSheet;
    tsParamsAndConditions: TTabSheet;
    tsConditionsDefs: TTabSheet;
    tsConsts: TTabSheet;
    pnlParamsTextSourceBUttons: TPanel;
    mmParamsTextSource: TMemo;
    rbtnPTSLine: TRadioButton;
    rbtnPTSWord: TRadioButton;
    rbtnPTSSubstr: TRadioButton;
    gbPTSSeporation: TGroupBox;
    edtPTSSubstr: TEdit;
    chbxPTSAllTrim: TCheckBox;
    chbxPTSParseMode_Auto: TCheckBox;
    sbtnPTSParseMode_Manual: TSpeedButton;
    tsParamsSettings: TTabSheet;
    chbxDeleteNonExistsCyclicParams: TCheckBox;
    chbxDeleteNonExistsConditionParams: TCheckBox;
    chbxDeleteNonExistsConstsParams: TCheckBox;
    ilParamsPC: TImageList;
    ilTemplatesCodeTC: TImageList;
    edtPTSWord: TEdit;
    fCyclParams: TframeCyclicParams;
    fCndParamsDefs: TframeConditionParamsDefs;
    fConstants: TframeConstants;
    btnInsertAndClose: TBitBtn;
    actGenerate: TAction;
    pnlParamsSettings: TPanel;
    pnlCodesAndParams: TPanel;
    pnlResults: TPanel;
    tcResults: TTabControl;
    pnlNotResults: TPanel;
    spltrResults: TSplitter;
    actTemplateCloseAll: TAction;
    actTemplateCloseAllExceptThis: TAction;
    actTemplateRemember: TAction;
    pnlResultsTabsButtons: TPanel;
    sbtnResultsSave: TSpeedButton;
    sbtnResultsCloseAll: TSpeedButton;
    sbtnResultsCloseAllExceptThis: TSpeedButton;
    actResultsSave: TAction;
    actResultsCloseAll: TAction;
    actResultsCloseAllExceptThis: TAction;
    actResultsCopyToClpBrd: TAction;
    actResultsInsert: TAction;
    actResultsInsertAndClose: TAction;
    actTempFrameAddCyclicParam: TAction;
    actTempFrameAddConditionParam: TAction;
    actTempFrameAddConst: TAction;
    actTempFrameAddSpacer: TAction;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure aeMessage(var Msg: tagMSG; var Handled: Boolean);
    procedure btnCloseClick(Sender: TObject);
    procedure sbtnToggleFormStateClick(Sender: TObject);
    procedure tcCodesChange(Sender: TObject);
    procedure tcCodesGetImageIndex(Sender: TObject; TabIndex: Integer; var ImageIndex: Integer);
    procedure tcCodesAllowCloseButton(Sender : TTabControl; const ATabIndex : integer; var Allow : boolean);
    procedure tcCodesCloseButtonClick(Sender : TTabControl; const ATabIndex : integer; var Allow, Handled : boolean);
    procedure btnToolsClick(Sender: TObject);
    procedure sbtnTemplateNewClick(Sender: TObject);
    procedure sbtnTemplateOpenClick(Sender: TObject);
    procedure actTemplateSaveExecute(Sender: TObject);
    procedure actTemplateSaveUpdate(Sender: TObject);
    procedure actTemplateCloseAllExecute(Sender: TObject);
    procedure actTemplateCloseAllUpdate(Sender: TObject);
    procedure actTemplateCloseAllExceptThisExecute(Sender: TObject);
    procedure actTemplateCloseAllExceptThisUpdate(Sender: TObject);
    procedure actTemplateRememberExecute(Sender: TObject);
    procedure actTemplateRememberUpdate(Sender: TObject);
    procedure actTempFrameAddCyclicParamExecute(Sender: TObject);
    procedure actTempFrameAddConditionParamExecute(Sender: TObject);
    procedure actTempFrameAddConstExecute(Sender: TObject);
    procedure actTempFrameAddSpacerExecute(Sender: TObject);
    procedure btnReCreateClick(Sender: TObject);
    procedure pcParamsChange(Sender: TObject);
    procedure pcParamsDrawTab(Control: TCustomTabControl; ATabIndex: Integer;
      const ARect: TRect; Active: Boolean);
    procedure chbxPTSParseMode_AutoClick(Sender: TObject);
    procedure sbtnPTSParseMode_ManualClick(Sender: TObject);
    procedure rbtnsPTSOnClick(Sender: TObject);
    procedure mmParamsTextSourceChange(Sender: TObject);
    procedure chbxDeleteNonExistsCyclicParamsClick(Sender: TObject);
    procedure chbxDeleteNonExistsConditionParamsClick(Sender: TObject);
    procedure chbxDeleteNonExistsConstsParamsClick(Sender: TObject);
    procedure edtPTSSubstrChange(Sender: TObject);
    procedure edtPTSWordChange(Sender: TObject);
    procedure chbxPTSAllTrimClick(Sender: TObject);
    procedure fCyclParamsdgViewDblClick(Sender: TObject);
    procedure pnlResultsResize(Sender: TObject);
    procedure tcResultsChange(Sender: TObject);
    procedure tcResultsCloseButtonClick(Sender : TTabControl; const ATabIndex : integer; var Allow, Handled : boolean);
    procedure actResultsSaveExecute(Sender: TObject);
    procedure actResultsSaveUpdate(Sender: TObject);
    procedure actResultsCloseAllExecute(Sender: TObject);
    procedure actResultsCloseAllExceptThisExecute(Sender: TObject);
    procedure actResultsCloseAllUpdate(Sender: TObject);
    procedure spltrCodeAndParamsMoved(Sender: TObject);
    procedure spltrResultsMoved(Sender: TObject);
    procedure pnlCodesAndParamsResize(Sender: TObject);
    procedure pnlClientResize(Sender: TObject);
    procedure actGenerateExecute(Sender: TObject);
    procedure actGenerateUpdate(Sender: TObject);
    procedure actResultsCopyToClpBrdExecute(Sender: TObject);
    procedure actResultsInsertExecute(Sender: TObject);
    procedure actResultsInsertAndCloseExecute(Sender: TObject);
    procedure actResultsInsertUpdate(Sender: TObject);

  private
    FCGDM : TCGDM;

    FCurTemplateCode : TTemplateCode;

    FFirstShowComplete : boolean;

    FResultsCounter : integer;

    FCurResultMemo : TMemo;

  private
    procedure CMShowingChanged(var Message: TMessage); message CM_SHOWINGCHANGED;

  protected
    procedure SetCGDM(ACGDM : TCGDM);

    procedure UpdateTemplates;

  private
    procedure Set_CurTemplateCode(const Value: TTemplateCode);
    function Get_PTSSeporationKind: TParamTextSourceSeporationKind;
    procedure Set_PTSSeporationKind(const Value: TParamTextSourceSeporationKind);
    procedure Set_CurResultMemo(const Value: TMemo);

  public
    function DeleteTemplateCode(const AIndex : integer) : boolean;
    function AddTemplateCode(const ATemplateName : string) : TTemplateCode;
    procedure RefreshTemplateCodes(const ANewCurTemplateCode : TTemplateCode = nil);

    procedure TemplateCodeFrameChanged(const ATemplateCode : TTemplateCode);

    procedure RefreshParameters;

    function AddResultMemo : TMemo;

  public
    property CGDM : TCGDM  read FCGDM;

    property CurTemplateCode : TTemplateCode  read FCurTemplateCode  write Set_CurTemplateCode;

    property PTSSeporationKind : TParamTextSourceSeporationKind  read Get_PTSSeporationKind  write Set_PTSSeporationKind;

    property CurResultMemo : TMemo  read FCurResultMemo  write Set_CurResultMemo;
  end;//TfrmCGMain
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
{$R *.res}
{$I TabControl_CloseButtons_Impl.inc}
//==============================================================================

type
  TWinControlHack = class(TWinControl);

//==============================================================================
// TSplitter
//==============================================================================
function TSplitter.Get_SpeedButton(AButton: TSplitterButton): TSpeedButton;
begin
  Result := TSpeedButton(FindComponent(GetEnumName(TypeInfo( TSplitterButton), ORD(AButton))));
end;//TSplitter.Get_SpeedButton
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TSplitter.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if FsbtnMiddle.Enabled then
    Exit;

  FParentHeight        := Parent.Height;
  FParentClientHeight  := Parent.ClientHeight;

  inherited;
end;//TSplitter.MouseDown
//==============================================================================
function TSplitter.BottonsExists: boolean;
begin
  Result := Assigned(FsbtnUp) and Assigned(FsbtnMiddle) and Assigned(FsbtnDown);
end;//TSplitter.BottonsExists
//==============================================================================
procedure TSplitter.SetParent(AParent: TWinControl);
begin
  inherited;

  if not BottonsExists then
    Exit;

  FsbtnUp.Parent      := AParent;
  FsbtnMiddle.Parent  := AParent;
  FsbtnDown.Parent    := AParent;
end;//TSplitter.SetParent
//==============================================================================
procedure TSplitter.Resize;
var
  vButton : TSplitterButton;
begin
  inherited;

  if not BottonsExists then
    Exit;


  for vButton := Low(TSplitterButton) to High(TSplitterButton) do
    with Buttons[vButton] do  begin
      Top     := Self.Top;
      Height  := Self.Height;
      Width   := Height * 3;

      Left := (Self.Width - Width) div 2 + (Ord(vButton) - 1) * Width;

      BringToFront;

      if not (Assigned(FTopControl) and Assigned(FBottomControl)) then
        Enabled := False
      else
        case vButton of
          sbUp:
            Enabled := FTopControl.Visible;

          sbMiddle:
            Enabled := (not FTopControl.Visible) or (not FBottomControl.Visible);

          sbDown:
            Enabled := FBottomControl.Visible;

        end;//case

    end;//with; for



end;//TSplitter.Resize
//==============================================================================
procedure TSplitter.Loaded;
begin
  inherited;

  Resize;
end;//TSplitter.Loaded
//==============================================================================
procedure TSplitter.BottonClick(Sender: TObject);
begin
  case TSplitterButton(TComponent(Sender).Tag) of
    sbUp:  begin
      FsbtnMiddle.Click;

      FTopControl.Hide;
      FBottomControlHeigth := FBottomControl.Height;
      FBottomControl.Height := Parent.ClientHeight - Height;
    end;//sbUp

    sbMiddle:
      if not FBottomControl.Visible then
        begin
          FBottomControl.Show;
          Self.Top := FBottomControl.Top - 1;
        end
      else
        begin
          FBottomControl.Height := FBottomControlHeigth;
          FTopControl.Show;
          FBottomControl.Top := FTopControl.Height + Self.Height;
        end;//else if; sbMiddle

    sbDown:  begin
      FsbtnMiddle.Click;
      FBottomControl.Hide;
    end;//sbDown

  end;//case

  Resize;
  TWinControlHack(Parent).Resize;
end;//TSplitter.BottonClick
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TSplitter.Create(AOwner: TComponent);
//------------------------------------------------------------------------------
  procedure CreateSpeedButton(var Field; const AButton : TSplitterButton);
  // Создать кнопку
  begin
    TSpeedButton(Field) := TSpeedButton.Create(Self);

    with TSpeedButton(Field) do  begin
      Name := GetEnumName(TypeInfo(TSplitterButton), ORD(AButton));

      Tag := Ord(AButton);

      Glyph.LoadFromResourceName( GetModuleHandle('SANCodeGeneratorPKG.bpl'), 'SAN_CG_SPLIT_' + AnsiUpperCase(Copy(Name, 3, Length(Name) - 2)));

      NumGlyphs := 2;

      OnClick := Self.BottonClick;

      case AButton of
        sbUp:
          Hint := 'Скрыть верхнюю часть';

        sbMiddle:
          Hint := 'Показывать верхнюю и нижнюю часть';

        sbDown:
          Hint := 'Скрыть нижнюю часть';

      end;//case

      ShowHint := True;
    end;//with
  end;//CreateSpeedButton
//------------------------------------------------------------------------------
begin
  inherited Create(AOwner);

  CreateSpeedButton(FsbtnUp,      sbUp);
  CreateSpeedButton(FsbtnMiddle,  sbMiddle);
  CreateSpeedButton(FsbtnDown,    sbDown);
end;//TSplitter.Create
//==============================================================================


//==============================================================================
// TfrmCGMain
//==============================================================================
// private
//==============================================================================
procedure TfrmCGMain.Set_CurTemplateCode(const Value: TTemplateCode);
begin
  if FCurTemplateCode = Value then
    Exit;

  if Assigned(CurTemplateCode) then
    TframeTemplateCode(CurTemplateCode.Frame).UnLinkToMainForm;

  FCurTemplateCode := Value;

  with tcCodes do
    TabIndex := Tabs.IndexOfObject(Value);

  UpdateTemplates;
end;//TfrmCGMain.Set_CurTemplateCode
//==============================================================================
procedure TfrmCGMain.CMShowingChanged(var Message: TMessage);
// При первом показе формы убирается Splash
begin
  inherited;

  if Visible and (not FFirstShowComplete) then  begin
    FFirstShowComplete := True;
//    CGDM.DestroySplash;;
  end;//if
end;//TfrmCGMain.CMShowingChanged
//==============================================================================
function TfrmCGMain.Get_PTSSeporationKind: TParamTextSourceSeporationKind;
begin
  if rbtnPTSLine.Checked then
    Result := ptsskLine
  else  if rbtnPTSWord.Checked then
    Result := ptsskWord
  else
    Result := ptsskSubString;
end;//TfrmCGMain.Get_PTSSeporationKind
//==============================================================================
procedure TfrmCGMain.Set_PTSSeporationKind(const Value: TParamTextSourceSeporationKind);
begin
  case Value of
    ptsskLine:
      rbtnPTSLine.Checked := True;

    ptsskWord:
      rbtnPTSWord.Checked := True;

    else
      rbtnPTSSubstr.Checked := True;
  end;//case
end;//TfrmCGMain.Set_PTSSeporationKind
//==============================================================================
procedure TfrmCGMain.Set_CurResultMemo(const Value: TMemo);
begin
  if FCurResultMemo = Value then
    Exit;

  if Assigned(FCurResultMemo) then
    FCurResultMemo.Hide;

  FCurResultMemo := Value;

  FCurResultMemo.Show;
end;//TfrmCGMain.Set_CurResultMemo
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TfrmCGMain.SetCGDM(ACGDM: TCGDM);
// Инициализация, когда уже назначен CGDM
begin
  FCGDM := ACGDM;

  sbtnToggleFormState.Visible := CGDM.RunModeAsApp;

  actTemplateRemember.Checked                           := CGDM.RememberTemplateCodes;
  edtPTSSubstr.Text                                     := CGDM.ParamTextSourceSepSubStr;
  edtPTSWord.Text                                       := CGDM.ParamTextSourceSepWordsEndChars;
  PTSSeporationKind                                     := CGDM.ParamTextSourceSeporationKind;
  chbxPTSParseMode_Auto.Checked                         := CGDM.ParamsTextSourceAutoParseMode;
  sbtnPTSParseMode_Manual.Enabled                       := not CGDM.ParamsTextSourceAutoParseMode;
  chbxDeleteNonExistsCyclicParams.Checked               := CGDM.ParamsDeleteNonExistsCyclic;
  chbxDeleteNonExistsConditionParams.Checked            := CGDM.ParamsDeleteNonExistsCondition;
  chbxDeleteNonExistsConstsParams.Checked               := CGDM.ParamsDeleteNonExistsConsts;
  chbxPTSAllTrim.Checked                                := CGDM.ParamsTrimAll;

  fCyclParams.ParamsComplex     := CGDM.ParamsComplex;
  fCndParamsDefs.ParamsComplex  := CGDM.ParamsComplex;
  fConstants.ParamsComplex      := CGDM.ParamsComplex;


  if CGDM.TemplateCodes.Count = 0 then
    AddTemplateCode('TemplateCode1.txt')
  else
    RefreshTemplateCodes(CGDM.TemplateCodes.First);
end;//TfrmCGMain.SetCGDM
//==============================================================================
procedure TfrmCGMain.UpdateTemplates;
begin
  if Assigned(CurTemplateCode) then
    TframeTemplateCode(CurTemplateCode.Frame).LinkToMainForm;

  CGDM.ParamsAddFromTemplateCode(CurTemplateCode);

  RefreshParameters;
end;//TfrmCGMain.UpdateTemplates
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TfrmCGMain.DeleteTemplateCode(const AIndex: integer) : boolean;
begin
  Result := False;

  with tcCodes do  begin

    if AIndex >= Tabs.Count then
      Exit;


    Components[AIndex].Free;

    CGDM.TemplateCodes.Delete(AIndex);
    FCurTemplateCode := nil;

    with Tabs do  try
      BeginUpdate;

      Delete(AIndex);

    finally
      EndUpdate;
    end;//t..f

    if AIndex < CGDM.TemplateCodes.Count then
      CurTemplateCode := CGDM.TemplateCodes[AIndex]
    else
      CurTemplateCode := CGDM.TemplateCodes.Last;
  end;//with


  Result := true;
end;//TfrmCGMain.DeleteTemplateCode
//==============================================================================
function TfrmCGMain.AddTemplateCode(const ATemplateName: string): TTemplateCode;
begin
  Result := CGDM.TemplateCodes.Add(CGDM.AppDataTemplatesFolder + '\' + ATemplateName);
  Result.Frame.Parent := tcCodes;

  with tcCodes do  begin
    Tabs.AddObject(ATemplateName + DupeString(' ', SPACES_IN_TAB), Result);
  end;//with

  CurTemplateCode := Result;
end;//TfrmCGMain.AddTemplate
//==============================================================================
procedure TfrmCGMain.RefreshTemplateCodes(const ANewCurTemplateCode : TTemplateCode);
var
  i : Integer;
begin
  with tcCodes, Tabs do  begin
    BeginUpdate;
    try
      Clear;

      for i := 0 to CGDM.TemplateCodes.Count - 1 do  begin
        Tabs.AddObject(ExtractFileName(CGDM.TemplateCodes[i].TemplateName) + DupeString(' ', SPACES_IN_TAB), CGDM.TemplateCodes[i]);
        CGDM.TemplateCodes[i].Frame.Parent := tcCodes;
        TframeTemplateCode(CGDM.TemplateCodes[i].Frame).UnLinkToMainForm;
      end;//for

      CurTemplateCode := ANewCurTemplateCode;
    finally
      EndUpdate;
    end;//t..f

  end;//with
end;//TfrmCGMain.RefreshTemplateCodes
//==============================================================================
procedure TfrmCGMain.TemplateCodeFrameChanged(const ATemplateCode: TTemplateCode);
begin
  if ATemplateCode <> CurTemplateCode then
    Exit;

  CGDM.ParamsAddFromTemplateCode(ATemplateCode);

  RefreshParameters;
end;//TfrmCGMain.TemplateCodeFrameChanged
//==============================================================================
procedure TfrmCGMain.RefreshParameters;
begin
  if CurTemplateCode = nil then
    Exit;


  fCyclParams.UpdateGrid;
  fCndParamsDefs.UpdateList;
  fConstants.UpdateGrid;

end;//RefreshParameters
//==============================================================================
function TfrmCGMain.AddResultMemo: TMemo;
// Добавить TMemo для отображения результатов генерации
begin
  Inc(FResultsCounter);


  Result := TMemo.Create(tcResults);
  with Result do  begin
    Name := Format('mmResult%d', [FResultsCounter]);

    Align := alClient;

    Parent := tcResults;

    Hint := Format('Результат генерации кода для шаблона'#13#10'«%s»', [ExtractFileName(CurTemplateCode.TemplateName)]);
    ShowHint := True;

    ScrollBars := ssBoth;

    Font.Size := 10;
    Font.Name := 'Courier New';

    Lines.Clear;
    Modified := False;
  end;//with


  tcResults.Tabs.InsertObject(0, ExtractFileName(CurTemplateCode.TemplateName) + DupeString(' ', SPACES_IN_TAB), Result);
  tcResults.TabIndex := 0;


  pnlNotResults.Hide;


  CurResultMemo := Result;
end;//TfrmCGMain.AddResultMemo
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TfrmCGMain.FormCreate(Sender: TObject);
// Инициализация, когда CGDM НЕ НАЗНАЧЕН!
begin
  sbtnTemplateCloseAllExceptThis.Glyph.TransparentColor  := clFuchsia;

  pcParams.ActivePageIndex := 0;

  tcCodes.OnAllowCloseButton := tcCodesAllowCloseButton;
  tcCodes.OnCloseButtonClick := tcCodesCloseButtonClick;

  tcResults.OnCloseButtonClick := tcResultsCloseButtonClick;

  spltrCodeAndParams.FTopControl     := pnlCodes;
  spltrCodeAndParams.FBottomControl  := pnlParams;
  spltrCodeAndParams.Resize;

  spltrResults.FTopControl     := pnlCodesAndParams;
  spltrResults.FBottomControl  := pnlResults;
  spltrResults.Resize;

end;//TfrmCGMain.FormCreate
//==============================================================================
procedure TfrmCGMain.FormDestroy(Sender: TObject);
begin

end;//TfrmCGMain.FormDestroy
//==============================================================================
procedure TfrmCGMain.FormActivate(Sender: TObject);
begin
  ae.OnMessage := aeMessage;
end;//TfrmCGMain.FormActivate
//==============================================================================
procedure TfrmCGMain.FormDeactivate(Sender: TObject);
begin
  ae.OnMessage := nil;
end;//TfrmCGMain.FormDeactivate
//==============================================================================
procedure TfrmCGMain.FormResize(Sender: TObject);
var
  vMinClientHeigth : integer;
begin
  vMinClientHeigth := pnlParamsBottons.Height +
                      pnlResults.Constraints.MinHeight*Ord(pnlResults.Visible) +
                      spltrResults.Height +
                      Ord(pnlCodesAndParams.Visible)*(
                        pnlCodes.Constraints.MinHeight*Ord(pnlCodes.Visible) +
                        spltrCodeAndParams.Height +
                        pnlParams.Constraints.MinHeight*Ord(pnlParams.Visible)
                      ) + 13
                      ;

  if ClientHeight < vMinClientHeigth then
    ClientHeight := vMinClientHeigth;

end;//TfrmCGMain.FormResize
//==============================================================================
procedure TfrmCGMain.aeMessage(var Msg: tagMSG; var Handled: Boolean);
begin

  case Msg.message of

    WM_LBUTTONDOWN:  begin

      // Клик вне фрейма fCyclParams - необходимо прекратить редактирование
      with fCyclParams do
        if not PtInRect(ClientRect, ScreenToClient(Msg.pt)) then
          CancelEdit;



    end;//WM_LBUTTONDOWN

  end;//case
end;//TfrmCGMain.aeMessage
//==============================================================================
procedure TfrmCGMain.btnCloseClick(Sender: TObject);
begin
  Close;
end;//TfrmCGMain.btnCloseClick
//==============================================================================
procedure TfrmCGMain.btnReCreateClick(Sender: TObject);
begin
  CGDM.ReCreateMainForm(True);
end;//TfrmCGMain.btnReCreateClick
//==============================================================================
procedure TfrmCGMain.sbtnToggleFormStateClick(Sender: TObject);
begin
  if (Sender as TSpeedButton).Down then
    FormStyle := fsStayOnTop
  else
    FormStyle := fsNormal;
end;//TfrmCGMain.sbtnToggleFormStateClick
//==============================================================================
procedure TfrmCGMain.btnToolsClick(Sender: TObject);
begin
  CGDM.CallToolsDLG;
end;//TfrmCGMain.btnToolsClick
//==============================================================================
procedure TfrmCGMain.tcCodesChange(Sender: TObject);
begin
  with (Sender as TTabControl) do
    CurTemplateCode := TTemplateCode(Tabs.Objects[TabIndex]);
end;//TfrmCGMain.tcCodesChange
//==============================================================================
procedure TfrmCGMain.tcCodesGetImageIndex(Sender: TObject; TabIndex: Integer;
  var ImageIndex: Integer);
begin
  with CGDM.TemplateCodes[TabIndex] do
    if FileExists(TemplateName) then
      ImageIndex := 1
    else
      ImageIndex := 0

end;//TfrmCGMain.tcCodesGetImageIndex
//==============================================================================
procedure TfrmCGMain.tcCodesAllowCloseButton(Sender: TTabControl;
  const ATabIndex: integer; var Allow: boolean);
begin
  Allow := Sender.Tabs.Count > 1;
end;//TfrmCGMain.tcCodesAllowCloseButton
//==============================================================================
procedure TfrmCGMain.tcCodesCloseButtonClick(Sender: TTabControl;
  const ATabIndex: integer; var Allow, Handled: boolean);
begin
  CGDM.TemplateCodes.Delete(ATabIndex);
  FCurTemplateCode := nil;

  if ATabIndex = Sender.TabIndex then
    if CGDM.TemplateCodes.Count > ATabIndex then
      CurTemplateCode := CGDM.TemplateCodes[ATabIndex]

    else if CGDM.TemplateCodes.Count > 0 then
      CurTemplateCode := CGDM.TemplateCodes[ATabIndex - 1]

    else
      CurTemplateCode := nil;
end;//TfrmCGMain.tcCodesCloseButtonClick
//==============================================================================
procedure TfrmCGMain.sbtnTemplateNewClick(Sender: TObject);
begin
  AddTemplateCode(Format('TemplateCode%d', [CGDM.TemplateCodes.Count + 1]));
end;//TfrmCGMain.sbtnTemplateNewClick
//==============================================================================
procedure TfrmCGMain.sbtnTemplateOpenClick(Sender: TObject);
var
  vTemplateCode : TTemplateCode;
begin
  if CGDM.OpenTemplateCode(vTemplateCode) = 0 then
    Exit;

  RefreshTemplateCodes(vTemplateCode);
end;//TfrmCGMain.sbtnTemplateOpenClick
//==============================================================================
procedure TfrmCGMain.actTemplateSaveExecute(Sender: TObject);
begin
  CGDM.SaveTemplateCode(CurTemplateCode);
end;//TfrmCGMain.actTemplateSaveExecute
//==============================================================================
procedure TfrmCGMain.actTemplateSaveUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(CurTemplateCode)
                             and Assigned(CurTemplateCode.TemplateText)
                             and (Trim(CurTemplateCode.TemplateText.Text) <> '');
end;//TfrmCGMain.actTemplateSaveUpdate
//==============================================================================
procedure TfrmCGMain.actTemplateCloseAllExecute(Sender: TObject);
begin
  tcCodes.Tabs.Clear;
  CGDM.TemplateCodes.Clear;
  FCurTemplateCode := nil;
  sbtnTemplateNew.Click;
end;//TfrmCGMain.actTemplateCloseAllExecute
//==============================================================================
procedure TfrmCGMain.actTemplateCloseAllUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (tcCodes.Tabs.Count > 1) or Assigned(CurTemplateCode) and (Trim(CurTemplateCode.TemplateText.Text) <> '');
end;//TfrmCGMain.actTemplateCloseAllUpdate
//==============================================================================
procedure TfrmCGMain.actTemplateCloseAllExceptThisExecute(Sender: TObject);
var
  i : integer;
begin
  with tcCodes do
    for i := Tabs.Count - 1 downto 0 do
      if i <> TabIndex then  begin
        Tabs.Objects[i].Free;
        Tabs.Delete(i);
      end;//if; for; with
end;//TfrmCGMain.actTemplateCloseAllExceptThisExecute
//==============================================================================
procedure TfrmCGMain.actTemplateCloseAllExceptThisUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (tcCodes.Tabs.Count > 1) or Assigned(CurTemplateCode) and (Trim(CurTemplateCode.TemplateText.Text) <> '');
end;//TfrmCGMain.actTemplateCloseAllExceptThisUpdate
//==============================================================================
procedure TfrmCGMain.actTemplateRememberExecute(Sender: TObject);
begin
  CGDM.RememberTemplateCodes := (Sender as TAction).Checked;
end;
//==============================================================================
procedure TfrmCGMain.actTemplateRememberUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (tcCodes.Tabs.Count > 1) or Assigned(CurTemplateCode) and (Trim(CurTemplateCode.TemplateText.Text) <> '');
end;//TfrmCGMain.actTemplateRememberUpdate
//==============================================================================
procedure TfrmCGMain.actTempFrameAddCyclicParamExecute(Sender: TObject);
begin
  TframeTemplateCode(CurTemplateCode.Frame).sbtnCyclicParam.Click;
end;//TfrmCGMain.actTempFrameAddCyclicParamExecute
//==============================================================================
procedure TfrmCGMain.actTempFrameAddConditionParamExecute(Sender: TObject);
begin
  TframeTemplateCode(CurTemplateCode.Frame).sbtnConditionParam.Click;
end;//TfrmCGMain.actTempFrameAddConditionParamExecute
//==============================================================================
procedure TfrmCGMain.actTempFrameAddConstExecute(Sender: TObject);
begin
  TframeTemplateCode(CurTemplateCode.Frame).sbtnConstParam.Click;
end;//TfrmCGMain.actTempFrameAddConstExecute
//==============================================================================
procedure TfrmCGMain.actTempFrameAddSpacerExecute(Sender: TObject);
begin
  TframeTemplateCode(CurTemplateCode.Frame).sbtnSpacer.Click;
end;//TfrmCGMain.actTempFrameAddSpacerExecute
//==============================================================================
procedure TfrmCGMain.pcParamsChange(Sender: TObject);
begin
  if (Sender as TPageControl).ActivePage = tsConditionsDefs then
    fCndParamsDefs.UpdateList;
end;//TfrmCGMain.pcParamsChange
//==============================================================================
procedure TfrmCGMain.pcParamsDrawTab(Control: TCustomTabControl;
  ATabIndex: Integer; const ARect: TRect; Active: Boolean);
begin
  with (Control as TPageControl), Canvas do  begin
    TextRect(ARect, ARect.Left + Images.Width + 10, ARect.Top + (RectHeight(ARect) - TextHeight('1')) div 2  + 1, Pages[ATabIndex].Caption);

    Images.Draw(Canvas, ARect.Left + 5, ARect.Top + (RectHeight(ARect) - Images.Height) div 2 + 2*Ord(not Active), Pages[ATabIndex].ImageIndex);
  end;//with
end;//TfrmCGMain.pcParamsDrawTab
//==============================================================================
procedure TfrmCGMain.chbxPTSParseMode_AutoClick(Sender: TObject);
begin
  sbtnPTSParseMode_Manual.Enabled := not (Sender as TCheckBox).Checked;

  CGDM.ParamsTextSourceAutoParseMode := (Sender as TCheckBox).Checked;
end;//TfrmCGMain.chbxPTSParseMode_AutoClick
//==============================================================================
procedure TfrmCGMain.sbtnPTSParseMode_ManualClick(Sender: TObject);
begin
  CGDM.ParseParamsTextSource;
end;//TfrmCGMain.sbtnPTSParseMode_ManualClick
//==============================================================================
procedure TfrmCGMain.rbtnsPTSOnClick(Sender: TObject);
begin
  edtPTSSubstr.Enabled  := rbtnPTSSubstr.Checked;
  edtPTSWord.Enabled    := rbtnPTSWord.Checked;

  CGDM.ParamTextSourceSeporationKind := TParamTextSourceSeporationKind((Sender as TRadioButton).Tag);
end;//TfrmCGMain.rbtnsPTSOnClick
//==============================================================================
procedure TfrmCGMain.mmParamsTextSourceChange(Sender: TObject);
begin
  if CGDM.ParamsTextSourceAutoParseMode then
    CGDM.ParseParamsTextSource;
end;//TfrmCGMain.mmParamsTextSourceChange
//==============================================================================
procedure TfrmCGMain.chbxDeleteNonExistsCyclicParamsClick(Sender: TObject);
begin
  CGDM.ParamsDeleteNonExistsCyclic := (Sender as TCheckBox).Checked;
end;//TfrmCGMain.chbxDeleteNonExistsCyclicParamsClick
//==============================================================================
procedure TfrmCGMain.chbxDeleteNonExistsConditionParamsClick(Sender: TObject);
begin
  CGDM.ParamsDeleteNonExistsCondition := (Sender as TCheckBox).Checked;
end;//TfrmCGMain.chbxDeleteNonExistsConditionParamsClick
//==============================================================================
procedure TfrmCGMain.chbxDeleteNonExistsConstsParamsClick(Sender: TObject);
begin
  CGDM.ParamsDeleteNonExistsConsts := (Sender as TCheckBox).Checked;
end;//TfrmCGMain.chbxDeleteNonExistsConstsParamsClick
//==============================================================================
procedure TfrmCGMain.edtPTSSubstrChange(Sender: TObject);
begin
  CGDM.ParamTextSourceSepSubStr := (Sender as TEdit).Text;
end;//TfrmCGMain.edtPTSSubstrChange
//==============================================================================
procedure TfrmCGMain.edtPTSWordChange(Sender: TObject);
begin
  CGDM.ParamTextSourceSepWordsEndChars := (Sender as TEdit).Text;
end;//TfrmCGMain.edtPTSWordChange
//==============================================================================
procedure TfrmCGMain.chbxPTSAllTrimClick(Sender: TObject);
begin
  CGDM.ParamsTrimAll := (Sender as TCheckBox).Checked;
end;//TfrmCGMain.chbxPTSAllTrimClick
//==============================================================================
procedure TfrmCGMain.fCyclParamsdgViewDblClick(Sender: TObject);
var
  vGridCoord : TGridCoord;
begin

  // Переход на список предзаданных значений при даблклике по условному параметру
  with (Sender as TDrawGrid), ScreenToClient(Mouse.CursorPos) do  begin
    vGridCoord := MouseCoord(X, Y);

    if (vGridCoord.Y = 0) and (vGridCoord.X > 1) then  begin
      pcParams.ActivePage := tsConditionsDefs;
      fCndParamsDefs.lbxCndParams.ItemIndex := vGridCoord.X - 2;
      fCndParamsDefs.mmDefaults.SetFocus;
    end;//if
  end;//with


end;//TfrmCGMain.fCyclParamsdgViewDblClick
//==============================================================================
procedure TfrmCGMain.pnlResultsResize(Sender: TObject);
begin
  pnlNotResults.Left  := ((Sender as TPanel).ClientWidth   - pnlNotResults.Width)   div 2;
  pnlNotResults.Top   := ((Sender as TPanel).ClientHeight  - pnlNotResults.Height)  div 2;
end;//TfrmCGMain.pnlResultsResize
//==============================================================================
procedure TfrmCGMain.tcResultsChange(Sender: TObject);
begin
  with (Sender as TTabControl) do  begin
    CurResultMemo := TMemo(Tabs.Objects[TabIndex]);

  end;//with
end;//TfrmCGMain.tcResultsChange
//==============================================================================
procedure TfrmCGMain.tcResultsCloseButtonClick(Sender: TTabControl;
  const ATabIndex: integer; var Allow, Handled: boolean);
begin
  Sender.Tabs.Objects[ATabIndex].Free;

  pnlNotResults.Visible := (Sender.Tabs.Count < 2);

  if ATabIndex = Sender.TabIndex then  begin
    FCurResultMemo := nil;
    Sender.OnChange(Sender);
  end;//if
end;//TfrmCGMain.tcResultsCloseButtonClick
//==============================================================================
procedure TfrmCGMain.actResultsSaveExecute(Sender: TObject);
begin
  CGDM.SaveResult(CurResultMemo.Lines);
end;//TfrmCGMain.actResultsSaveExecute
//==============================================================================
procedure TfrmCGMain.actResultsSaveUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (tcResults.Tabs.Count > 0) and (Trim(CurResultMemo.Text) <> '');
end;//TfrmCGMain.actResultsSaveUpdate
//==============================================================================
procedure TfrmCGMain.actResultsCloseAllExecute(Sender: TObject);
var
  i : integer;
begin
  with tcResults.Tabs do  begin
    BeginUpdate;
    try

      for i := 0 to Count - 1 do
        Objects[i].Free;

      Clear;

      pnlNotResults.Visible := (Count < 2);
    finally
      EndUpdate;
    end;//t..f
  end;//with

end;//TfrmCGMain.actResultsCloseAllExecute
//==============================================================================
procedure TfrmCGMain.actResultsCloseAllUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (tcResults.Tabs.Count > 0);
end;//TfrmCGMain.actResultsCloseAllUpdate
//==============================================================================
procedure TfrmCGMain.actResultsCloseAllExceptThisExecute(Sender: TObject);
var
  i : integer;
begin
  with tcResults.Tabs do  begin
    BeginUpdate;
    try

      i := 0;
      while i <= Count - 1 do
        if Objects[i] <> CurResultMemo then
          begin
            Objects[i].Free;
            Delete(i);
          end
        else
          Inc(i);

      pnlNotResults.Visible := (Count < 2);
    finally
      EndUpdate;
    end;//t..f
  end;//with
end;//TfrmCGMain.actResultsCloseAllExceptThisExecute
//==============================================================================
procedure TfrmCGMain.actResultsCopyToClpBrdExecute(Sender: TObject);
begin
  with CurResultMemo do  begin
    SelectAll;
    CopyToClipboard;
  end;//with
end;//TfrmCGMain.actResultsCopyToClpBrdExecute
//==============================================================================
procedure TfrmCGMain.actResultsInsertExecute(Sender: TObject);
begin
  CGDM.OnInsertResult(CurResultMemo.Lines);
end;//TfrmCGMain.actResultsInsertExecute
//==============================================================================
procedure TfrmCGMain.actResultsInsertUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(CGDM.OnInsertResult) and (tcResults.Tabs.Count > 0) and (Trim(CurResultMemo.Text) <> '');
end;//TfrmCGMain.actResultsInsertUpdate
//==============================================================================
procedure TfrmCGMain.actResultsInsertAndCloseExecute(Sender: TObject);
begin
  CGDM.OnInsertResult(CurResultMemo.Lines);

  Close;
end;//TfrmCGMain.actResultsInsertAndCloseExecute
//==============================================================================
procedure TfrmCGMain.spltrCodeAndParamsMoved(Sender: TObject);
begin
  with (Sender as TSplitter) do  begin

    if Top < pnlCodes.Height then  begin
      pnlParams.Height := FParentClientHeight - pnlCodes.Height - Height;

      Top := pnlCodes.Height + 1;
    end;//if

  end;//with
end;//TfrmCGMain.spltrCodeAndParamsMoved
//==============================================================================
procedure TfrmCGMain.spltrResultsMoved(Sender: TObject);
begin
  with (Sender as TSplitter) do  begin

    if Top < pnlCodesAndParams.Height then  begin
      pnlParams.Height := pnlParams.Constraints.MinHeight;
      pnlCodes.Height  := pnlCodes.Constraints.MinHeight;

      pnlResults.Height := FParentClientHeight - Height - pnlCodesAndParams.Constraints.MinHeight;

      Top := pnlCodesAndParams.Height + 1;


      Self.ClientHeight := pnlButtons.Height + FParentHeight;


    end;//if

  end;//with
end;//TfrmCGMain.spltrResultsMoved
//==============================================================================
procedure TfrmCGMain.pnlCodesAndParamsResize(Sender: TObject);
begin
  case pnlCodes.Visible of
    False:
      if spltrCodeAndParams.Top <> 0 then
        pnlParams.Height := (Sender as TPanel).ClientHeight - spltrCodeAndParams.Height;

    True:
      if spltrCodeAndParams.Top < pnlCodes.Height then
        pnlParams.Height := (Sender as TPanel).ClientHeight - pnlCodes.Height - spltrCodeAndParams.Height;

  end;//case
end;//TfrmCGMain.pnlCodesAndParamsResize
//==============================================================================
procedure TfrmCGMain.pnlClientResize(Sender: TObject);
begin
  case pnlCodesAndParams.Visible of
    False:
      if spltrResults.Top <> 0 then
        pnlResults.Height := (Sender as TPanel).ClientHeight - spltrResults.Height;

    True:
      if spltrResults.Top < pnlCodesAndParams.Height then
        pnlResults.Height := (Sender as TPanel).ClientHeight - pnlCodesAndParams.Height - spltrResults.Height;

  end;//case
end;//TfrmCGMain.pnlClientResize
//==============================================================================
procedure TfrmCGMain.actGenerateExecute(Sender: TObject);
begin
  with AddResultMemo do
    CGDM.GenerateFor(CurTemplateCode, Lines);
end;//TfrmCGMain.actGenerateExecute
//==============================================================================
procedure TfrmCGMain.actGenerateUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(CurTemplateCode)
                             and Assigned(CurTemplateCode.TemplateText)
                             and (Trim(CurTemplateCode.TemplateText.Text) <> '');
end;//TfrmCGMain.actGenerateUpdate
//==============================================================================


END.
