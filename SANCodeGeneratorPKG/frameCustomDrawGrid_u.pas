UNIT frameCustomDrawGrid_u;/////////////////////////////////////////////////////
// Абстрактный фрейм с DrawGrid-ом
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CGDM_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Menus, Types, Math, ImgList, StdCtrls;
//==============================================================================
TYPE
  TFDGEditorType = (fdgetNone, fdgetEdit, fdgetComboBox, fdgetMemo);
//==============================================================================
  TDrawGrid = class(Grids.TDrawGrid)
  private
    FSaveFont   : TFont;
    FSavePen    : TPen;
    FSaveBrush  : TBrush;

    FDCTStrings : TStrings;

  protected
    procedure Paint; override;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;

    procedure DoDrawCellText(const ARect : TRect; const AText : string;
      const AAlignment : TAlignment; const AHrzIndent : integer;
      const ALayout : TTextLayout;   const AVrtIndent : integer);


  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

  end;//TDrawGrid
//==============================================================================
  TframeCustomDrawGrid = class(TFrame)
    dgView: TDrawGrid;
    pmDGView: TPopupMenu;
    ilPMDGView: TImageList;
    edtEditer: TEdit;
    cmbxEditer: TComboBox;
    mmEditer: TMemo;

    procedure dgViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dgViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dgViewMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure EditersExit(Sender: TObject);
    procedure EditersKeyPress(Sender: TObject; var Key: Char);
    procedure mmEditerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    // Координаты последнего MouseDown
    FMDGridCoord : TGridCoord;

    // ИНФОРМАЦИЯ О ПОСЛЕДНЕМ РЕДАКТИРОВАНИИ
      // Флаг отмены редактирования
      FEditerCancel : boolean;

      // Координаты ячейки редактирования
      FEditerCol, FEditerRow : integer;

      // Тип редактора
      FEditerType : TFDGEditorType;

  private
    function Get_Editers(const AEditerType: TFDGEditorType): TWinControl;
    function Get_EditersTexts(const AEditerType: TFDGEditorType): string;

  protected
    function GridIsEmpty : boolean; virtual; abstract;

    procedure GetCellHint(const ACol, ARow : integer; var ACellHint : string); virtual;

    procedure GetCellEditer(const ACol, ARow : integer; var AEditerType : TFDGEditorType;
      var AFullEditerText, ASelEditerText : string); virtual;

    procedure SetCellEditerText(const ACol, ARow : integer; AEditerText : string); virtual;

  protected
    property MDGridCoord : TGridCoord  read FMDGridCoord;

  public
    constructor Create(AOwner: TComponent); override;

    procedure DrawCellText(const ARect : TRect; AText : string; const ADefIfEmpty : string = '';
      const AAlignment : TAlignment = taCenter; const AHrzIndent : integer = 0;
      const ALayout : TTextLayout = tlCenter; const AVrtIndent : integer = 0);

    procedure EditCell(const ACol, ARow : integer);
    procedure EditCellComplete;
    procedure CancelEdit;

    procedure UpdateGrid; virtual; abstract;

  public
    property Editers[const AEditerType : TFDGEditorType] : TWinControl  read Get_Editers;
    property EditersTexts[const AEditerType : TFDGEditorType] : string  read Get_EditersTexts;

  end;//TframeCustomDrawGrid
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
// TDrawGrid
//==============================================================================
// protected
//==============================================================================
procedure TDrawGrid.Paint;
// Пустая строка затирается
var
  vBrushColor : TColor;
  vRect : TRect;
begin
  inherited;

  if TframeCustomDrawGrid(Owner).GridIsEmpty then
    with Canvas do  begin
      vBrushColor := Brush.Color;

      try
        Brush.Color := Self.Color;
        vRect.TopLeft      := CellRect(0, Row).TopLeft;
        vRect.BottomRight  := CellRect(ColCount - 1, Row).BottomRight;
        Inc(vRect.Right);
        Inc(vRect.Bottom);
        FillRect(vRect);
      finally
        Brush.Color := vBrushColor;
      end;//t..f
    end;//if
end;//TDrawGrid.Paint
//==============================================================================
procedure TDrawGrid.DrawCell(ACol, ARow: Integer; ARect: TRect; AState: TGridDrawState);
// Если строка не пустая, то сохраняются параметры холста
begin

  if TframeCustomDrawGrid(Owner).GridIsEmpty and (ARow = Row) then
    Exit;


  with Canvas do  try
    FSaveFont.Assign(Font);
    FSavePen.Assign(Pen);
    FSaveBrush.Assign(Brush);

    inherited;

  finally
    Font.Assign(FSaveFont);
    Pen.Assign(FSavePen);
    Brush.Assign(FSaveBrush);
  end;//t..f
end;//TDrawGrid.DrawCell
//==============================================================================
procedure TDrawGrid.DoDrawCellText(const ARect: TRect; const AText: string;
  const AAlignment: TAlignment; const AHrzIndent : integer;
  const ALayout : TTextLayout; const AVrtIndent : integer);
// Отрисовка многострочного текста
var
  i, vTextHeight, X, Y : integer;
  vSaveBrushStyle : TBrushStyle;
begin

  if AText = '' then
    Exit;


  FDCTStrings.Text := AText;


  with Canvas do  begin
    vSaveBrushStyle := Brush.Style;

    try
      Brush.Style := bsClear;

      vTextHeight := TextHeight(FDCTStrings[0]);

      for i := 0 to FDCTStrings.Count - 1 do  begin

        // Оx
        case AAlignment of
          taLeftJustify:
            X := ARect.Left + AHrzIndent;

          taRightJustify:
            X := ARect.Right - AHrzIndent - TextWidth(FDCTStrings[i]);

          else
            X := ARect.Left + (RectWidth(ARect) - TextWidth(FDCTStrings[i])) div 2;

        end;//case


        // Оy
        case ALayout of
          tlTop:
            Y := ARect.Top + AVrtIndent + i*vTextHeight;

          tlBottom:
            Y := ARect.Bottom - AVrtIndent - (FDCTStrings.Count - i)*vTextHeight;

          else
            Y := ARect.Top + (RectHeight(ARect) - FDCTStrings.Count*vTextHeight) div 2 + vTextHeight*i;

        end;//case



        TextRect(ARect, X, Y, FDCTStrings[i]);
      end;//for

    finally
      Brush.Style := vSaveBrushStyle;
    end;//t..f
  end;//with
end;//TDrawGrid.DoDrawCellText
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TDrawGrid.Create(AOwner: TComponent);
begin
  inherited;

  FSaveFont   := TFont.Create;
  FSavePen    := TPen.Create;
  FSaveBrush  := TBrush.Create;

  FDCTStrings := TStringList.Create;

end;//TDrawGrid.Create
//==============================================================================
destructor TDrawGrid.Destroy;
begin
  FreeAndNil(FSaveFont);
  FreeAndNil(FSavePen);
  FreeAndNil(FSaveBrush);

  FreeAndNil(FDCTStrings);

  inherited;
end;//TDrawGrid.Destroy
//==============================================================================

//==============================================================================
// TframeCustomDrawGrid
//==============================================================================
function TframeCustomDrawGrid.Get_Editers(const AEditerType: TFDGEditorType): TWinControl;
begin
  case AEditerType of
    fdgetEdit:
      Result := edtEditer;

    fdgetComboBox:
      Result := cmbxEditer;

    fdgetMemo:
      Result := mmEditer;

    else
      Result := nil;
  end;//case
end;//TframeCustomDrawGrid.Get_Editers
//==============================================================================
function TframeCustomDrawGrid.Get_EditersTexts(const AEditerType: TFDGEditorType): string;
begin
  case AEditerType of
    fdgetEdit:
      Result := edtEditer.Text;

    fdgetComboBox:
      Result := cmbxEditer.Text;

    fdgetMemo:
      Result := mmEditer.Text;

    else
      Result := '';
  end;//case
end;//TframeCustomDrawGrid.Get_EditersTexts
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TframeCustomDrawGrid.GetCellHint(const ACol, ARow: integer; var ACellHint: string);
begin
end;//TframeCustomDrawGrid.GetCellHint
//==============================================================================
procedure TframeCustomDrawGrid.GetCellEditer(const ACol, ARow: integer;
  var AEditerType: TFDGEditorType; var AFullEditerText, ASelEditerText: string);
begin
end;//TframeCustomDrawGrid.GetCellEditer
//==============================================================================
procedure TframeCustomDrawGrid.SetCellEditerText(const ACol, ARow: integer;
  AEditerText: string);
begin
end;//TframeCustomDrawGrid.SetCellEditerText
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeCustomDrawGrid.Create(AOwner: TComponent);
var
  vEditorType : TFDGEditorType;
begin
  inherited;

  for vEditorType := Succ(Low(TFDGEditorType)) to High(TFDGEditorType) do
    with Editers[vEditorType] do  begin
      Height  := dgView.DefaultRowHeight;
      Top     := -100;
    end;//with; for


end;//TframeCustomDrawGrid.Create
//==============================================================================
procedure TframeCustomDrawGrid.DrawCellText(const ARect: TRect; AText: string;
  const ADefIfEmpty: string; const AAlignment: TAlignment; const AHrzIndent : integer;
  const ALayout : TTextLayout; const AVrtIndent : integer);
begin
  if AText = '' then
    AText := ADefIfEmpty;

  dgView.DoDrawCellText(ARect, AText, AAlignment, AHrzIndent, ALayout, AVrtIndent);
end;//TframeCustomDrawGrid.DrawCellText
//==============================================================================
procedure TframeCustomDrawGrid.EditCell(const ACol, ARow: integer);
var
  vEditerType : TFDGEditorType;
  vFullEditerText, vSelEditerText : string;
  vPos : integer;
  vEditerRect : TRect;
begin
  vEditerType      := fdgetNone;
  vFullEditerText  := '';
  vSelEditerText   := '';


  GetCellEditer(ACol, ARow, vEditerType, vFullEditerText, vSelEditerText);


  // Частности (Текст и выделение)
  case vEditerType of
    fdgetNone:
      Exit;

    fdgetEdit:
      with edtEditer do  begin
        Text := vFullEditerText;

        vPos := Pos(vSelEditerText, Text);

        if vPos > 0 then  begin
          SelStart   := vPos - 1;
          SelLength  := Length(vSelEditerText);
        end;//if
      end;//with;fdgetEdit

    fdgetComboBox:
      with cmbxEditer do  begin
        Items.Text := vFullEditerText;

        ItemIndex := Items.IndexOf(vSelEditerText);

        if ItemIndex = -1 then
          Text := vSelEditerText;
      end;//with;fdgetComboBox

    fdgetMemo:
      with mmEditer do  begin
        Text := vFullEditerText;

        vPos := Pos(vSelEditerText, Text);

        if vPos > 0 then  begin
          SelStart   := vPos - 1;
          SelLength  := Length(vSelEditerText);
        end;//if
      end;//with;fdgetMemo

  end;//case


  FEditerCancel  := False;
  FEditerCol     := ACol;
  FEditerRow     := ARow;
  FEditerType    := vEditerType;


  // Область ячейки в координатах фрейма
  with dgView do  begin
    vEditerRect := CellRect(MIN(ACol, ColCount - 1), MIN(ARow, RowCount - 1));

    OffsetRect(
      vEditerRect,
      IfThen(ACol > ColCount - 1, RectWidth( vEditerRect) + GridLineWidth),
      IfThen(ARow > RowCount - 1, RectHeight(vEditerRect) + GridLineWidth)
    );

    vEditerRect.TopLeft      := ClientToScreen(vEditerRect.TopLeft);
    vEditerRect.BottomRight  := ClientToScreen(vEditerRect.BottomRight);
  end;//with
  vEditerRect.TopLeft      := Self.ScreenToClient(vEditerRect.TopLeft);
  vEditerRect.BottomRight  := Self.ScreenToClient(vEditerRect.BottomRight);


  // Положение над гридом
  with Editers[vEditerType] do  begin
    Left    := vEditerRect.Left;
    Top     := vEditerRect.Top;

    Width   := RectWidth(vEditerRect);
    Height  := RectHeight(vEditerRect);

    Show;
    SetFocus;
  end;//with

end;//TframeCustomDrawGrid.EditCell
//==============================================================================
procedure TframeCustomDrawGrid.EditCellComplete;
begin

  if FEditerCancel then
    Exit;

  SetCellEditerText(FEditerCol, FEditerRow, EditersTexts[FEditerType]);

  dgView.Repaint;

  FEditerType := fdgetNone;
end;//TframeCustomDrawGrid.EditCellComplete
//==============================================================================
procedure TframeCustomDrawGrid.CancelEdit;
begin
  FEditerCancel := True;

  if FEditerType <> fdgetNone then
    Editers[FEditerType].Hide;
end;//TframeCustomDrawGrid.CancelEdit
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeCustomDrawGrid.dgViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  with (Sender as TDrawGrid) do
    case Key of

      VK_APPS:
        with ClientToScreen(CenterPoint(CellRect(Col, Row))) do
          pmDGView.Popup(X, Y);

      VK_RETURN:
        if not GridIsEmpty then
          EditCell(Col, Row);

    end;//case
end;//TframeCustomDrawGrid.dgViewKeyDown
//==============================================================================
procedure TframeCustomDrawGrid.dgViewMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  vGridCoord : TGridCoord;
begin
  with (Sender as TDrawGrid) do  begin
    vGridCoord := MouseCoord(X, Y);
    FMDGridCoord := vGridCoord;

    case Button of
      mbLeft:
        if (not GridIsEmpty) and (vGridCoord.X = Col) and (vGridCoord.Y = Row) then
          EditCell(Col, Row);

      mbRight: begin

        if not((vGridCoord.X < 0) or (vGridCoord.Y <= 0)) then  begin
          Col := MAX(vGridCoord.X, FixedCols);
          Row := MAX(vGridCoord.Y, FixedRows);
        end;//if

        with ClientToScreen(Point(X, Y)) do
          pmDGView.Popup(X, Y);

      end;//mbRight

      mbMiddle: ;

    end;//case

  end;//with
end;//TframeCustomDrawGrid.dgViewMouseDown
//==============================================================================
procedure TframeCustomDrawGrid.dgViewMouseMove(Sender: TObject;  Shift: TShiftState;
  X, Y: Integer);
var
  vNewHint : string;
begin
  vNewHint := '';

  with Sender as TDrawGrid do  begin


    with MouseCoord(X, Y) do
      GetCellHint(X, Y, vNewHint);


    if vNewHint <> Hint then  begin
      Application.CancelHint;
      Hint := vNewHint;
    end;//if

  end;//with
end;//TframeCustomDrawGrid.dgViewMouseMove
//==============================================================================
procedure TframeCustomDrawGrid.EditersExit(Sender: TObject);
begin
  with (Sender as TWinControl) do  begin
    EditCellComplete;

    Hide;
  end;//with
end;//TframeCustomDrawGrid.EditersExit
//==============================================================================
procedure TframeCustomDrawGrid.EditersKeyPress(Sender: TObject; var Key: Char);
begin

  if (Sender is TComboBox) and (Sender as TComboBox).DroppedDown then
    Exit;


  if CharInSet(Key, [#13, #27]) then  begin
    case Key of
      #13:
        if (Sender is TMemo) then
          Exit
        else
          dgView.SetFocus;

      #27:  begin
        FEditerCancel := True;
        dgView.SetFocus;
      end;//#27

    end;//case

    Key := #0;
  end;//if
end;//TframeCustomDrawGrid.EditersKeyPress
//==============================================================================
procedure TframeCustomDrawGrid.mmEditerKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) then  begin
    dgView.SetFocus;
    Key := 0;
  end;//if
end;//TframeCustomDrawGrid.mmEditerKeyDown
//==============================================================================

END.
