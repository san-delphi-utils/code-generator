UNIT frmToolsDLG_u;/////////////////////////////////////////////////////////////
// Диалог настроек
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  frameScriptSign_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, Buttons, ExtCtrls;
//==============================================================================
TYPE
  TfrmToolsDLG = class(TForm)
    pnlButtons: TPanel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    actList: TActionList;
    actOK: TAction;
    fSSCyclicParam: TframeScriptSign;
    fSSConditionParam: TframeScriptSign;
    fSSSpacer: TframeScriptSign;
    fSSConstParam: TframeScriptSign;

    procedure actOKUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private

  public

  end;//TfrmToolsDLG
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
{$R *.dfm}
//==============================================================================
//Циклический параметр
//Условный параметр
//Выравнивающие пробелы
//==============================================================================
// TfrmToolsDLG
//==============================================================================
// published
//==============================================================================
procedure TfrmToolsDLG.FormCreate(Sender: TObject);
begin
  fSSCyclicParam.Sign.IsRanged := False;
end;//TfrmToolsDLG.FormCreate
//==============================================================================
procedure TfrmToolsDLG.actOKUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := True;
end;//TfrmToolsDLG.actOKUpdate
//==============================================================================

END.
