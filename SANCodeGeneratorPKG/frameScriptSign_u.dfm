object frameScriptSign: TframeScriptSign
  Left = 0
  Top = 0
  Width = 320
  Height = 50
  Constraints.MaxHeight = 50
  Constraints.MaxWidth = 320
  Constraints.MinHeight = 50
  Constraints.MinWidth = 320
  TabOrder = 0
  object gbScriptSign: TGroupBox
    Left = 0
    Top = 0
    Width = 320
    Height = 50
    Align = alClient
    Caption = 'gbScriptSign'
    TabOrder = 0
    DesignSize = (
      320
      50)
    object lblSignBeg: TLabel
      Left = 13
      Top = 20
      Width = 41
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1086':'
    end
    object lblColor: TLabel
      Left = 230
      Top = 18
      Width = 7
      Height = 16
      Caption = '$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblSignEnd: TLabel
      Left = 122
      Top = 20
      Width = 35
      Height = 13
      Caption = #1050#1086#1085#1077#1094':'
    end
    object edtSignBeg: TEdit
      Left = 60
      Top = 17
      Width = 49
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = edtSignBegChange
    end
    object edtColor: TEdit
      Left = 238
      Top = 17
      Width = 68
      Height = 22
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 6
      ParentFont = False
      TabOrder = 1
      Text = 'EDTCOLOR'
      OnChange = edtColorChange
      OnKeyPress = edtColorKeyPress
      OnMouseMove = edtColorMouseMove
    end
    object edtSignEnd: TEdit
      Left = 163
      Top = 17
      Width = 49
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnChange = edtSignEndChange
    end
  end
  object dlgColor: TColorDialog
    Options = [cdFullOpen]
    Left = 248
    Top = 8
  end
end
