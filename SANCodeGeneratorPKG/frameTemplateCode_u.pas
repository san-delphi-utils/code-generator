UNIT frameTemplateCode_u;///////////////////////////////////////////////////////
// Фрейм для шаблона кода
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CGDM_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Buttons, StrUtils;
//==============================================================================
TYPE
  TframeTemplateCode = class(TFrame)
    pnlTemplate: TPanel;
    reTemplate: TRichEdit;
    sbtnCyclicParam: TSpeedButton;
    sbtnConditionParam: TSpeedButton;
    sbtnSpacer: TSpeedButton;
    sbtnConstParam: TSpeedButton;
    sbTemplate: TStatusBar;
    procedure reTemplateChange(Sender: TObject);
    procedure reTemplateSelectionChange(Sender: TObject);
    procedure reTemplateKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbtnCyclicParamClick(Sender: TObject);
    procedure sbtnConditionParamClick(Sender: TObject);
    procedure sbtnSpacerClick(Sender: TObject);
    procedure sbtnConstParamClick(Sender: TObject);

  private
    FTemplateCode : TTemplateCode;

    FParamsComplex : TParamsComplex;

  private
    function Get_CGDM: TCGDM;

  protected
    procedure LinkToTemplateCode(const ATemplateCode: TTemplateCode);

    procedure AddScriptSign(ASign : TScriptSign);
    procedure MarkScriptSign(ASign : TScriptSign);
    procedure MarkTemplateCode;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    procedure LinkToMainForm;
    procedure UnLinkToMainForm;

  public
    property TemplateCode : TTemplateCode  read FTemplateCode;
    property CGDM : TCGDM  read Get_CGDM;
    property ParamsComplex : TParamsComplex  read FParamsComplex;
  end;//TframeTemplateCode
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  frmCGMain_u;
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
type
  TTemplateCodeHack = class(TTemplateCode);
//==============================================================================
// TframeTemplateCode
//==============================================================================
// private
//==============================================================================
function TframeTemplateCode.Get_CGDM: TCGDM;
begin
  Result := TCGDM(Owner);
end;//TframeTemplateCode.Get_CGDM
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TframeTemplateCode.LinkToTemplateCode(const ATemplateCode: TTemplateCode);
begin
  FTemplateCode := ATemplateCode;
end;//TframeTemplateCode.LinkToTemplateCode
//==============================================================================
procedure TframeTemplateCode.AddScriptSign(ASign: TScriptSign);
// Добавить знак
var
  vSelStart : integer;
begin
  with reTemplate do  begin
    vSelStart  := SelStart;
    SelText    := ASign.SignBeg + ASign.SignEnd;
    SelStart   := vSelStart + Length(ASign.SignBeg);
    SelLength  := 0;
  end;//with
end;//TframeTemplateCode.AddScriptSign
//==============================================================================
procedure TframeTemplateCode.MarkScriptSign(ASign: TScriptSign);
// Разметить все знаки ASign в тексте
type
  PParamRec = ^TParamRec;
  TParamRec = record
    Sign  : TScriptSign;
    Frame : TframeTemplateCode;
  end;//TParamRec
//------------------------------------------------------------------------------
  function GetStringsLineTotalPos(const AStrings: TStrings; const ALine : integer) : integer;
  // Позиция последнего символа в AStrings перед ALine
  var
    i : integer;
  begin
    Result := 0;

    for i := 0 to ALine - 1 do
      Inc(Result, Length(AStrings[i]) + 2);
  end;//GetStringsLineTotalPos
//------------------------------------------------------------------------------
  procedure MarkProc(const ASignBeg, ASignEnd : string; AStrings: TStrings;
    var ALine, ACol, AOffset : integer; const AParamName: string;
    var AComplete : boolean; AParamRec : PParamRec);
  // Размечающая процедура обратного вызова
  begin

    // Добавление параметра в список
    AParamRec^.Frame.ParamsComplex.RegisterParam(AParamRec^.Sign, AParamName);


    with AParamRec^.Frame.reTemplate do  begin
      SelStart   := GetStringsLineTotalPos(AStrings, ALine) + ACol - 1 - ALine;
      SelLength  := Length(ASignBeg) + Length(AParamName) + Length(ASignEnd);
      SelAttributes.Style := [fsBold];
      SelAttributes.Color := AParamRec^.Sign.Color;
    end;//with

  end;//Spacers_MAXCol
//------------------------------------------------------------------------------
var
  vParamRec : TParamRec;
begin
  vParamRec.Sign   := ASign;
  vParamRec.Frame  := Self;

  EnumStrFragments(ASign.SignBeg, ASign.SignEnd, reTemplate.Lines, @MarkProc, @vParamRec);

end;//TframeTemplateCode.MarkScriptSign
//procedure TframeTemplateCode.MarkScriptSign(ASign: TScriptSign);
//// Разметить все знаки ASign в тексте
//var
//  vLine, vTotalCharsCount, vLineCharsCount,
//  vPosBeg, vPosEnd, vLengthSignBeg, vLengthSignEnd, vSelLength : integer;
//  vParamName, S, SEnd : string;
//begin
//  vLengthSignBeg    := Length(ASign.SignBeg);
//  vLengthSignEnd    := Length(ASign.SignEnd);
//  vTotalCharsCount  := 0;
//  vParamName        := '';
//
//  with reTemplate do  begin
//
//    for vLine := 0 to Lines.Count - 1 do  begin
//      vLineCharsCount := 0;
//      S := Lines[vLine];
//
//      repeat
//        vPosBeg := POS(ASign.SignBeg, S);
//
//
//        if vPosBeg = 0 then
//          Break;
//
//
//        Inc(vLineCharsCount, vPosBeg);
//
//        vSelLength := vLengthSignBeg;
//
//
//        if ASign.IsRanged then  begin
//          SEnd := S;
//          System.Delete(SEnd, 1, vPosBeg + vLengthSignBeg - 1);
//
//          vPosEnd := POS(ASign.SignEnd, SEnd);
//
//          if vPosEnd = 0 then
//            begin
//              vParamName := SEnd;
//              Inc(vSelLength, Length(SEnd) - 1);
//            end
//          else
//            begin
//              vParamName := Copy(SEnd, 1, vPosEnd - 1);
//              Inc(vSelLength, vPosEnd + vLengthSignEnd - 1);
//            end;//else if
//        end;//if
//
//
//        // Добавление параметра в список
//        ParamsComplex.RegisterParam(ASign, vParamName);
//
//
//        SelStart   := vTotalCharsCount + vLineCharsCount + vLine - 1;
//        SelLength  := vSelLength;
//        SelAttributes.Style := [fsBold];
//        SelAttributes.Color := ASign.Color;
//
//        System.Delete(S, 1, vPosBeg);
//      until FALSE;
//
//      Inc(vTotalCharsCount, Length(Lines[vLine]));
//    end;//for
//
//
//  end;//with
//end;//TframeTemplateCode.MarkScriptSign
//==============================================================================
procedure TframeTemplateCode.MarkTemplateCode;
// Разметить текст
var
  vSaveSelStart, vSaveSelLength : integer;
  vSaveSelAttrColor : TColor;
  vSaveREOnChange : TNotifyEvent;
begin
  ParamsComplex.Clear;

  with reTemplate do  begin
    vSaveREOnChange := OnChange;

    OnChange := nil;
    try
      vSaveSelStart      := SelStart;
      vSaveSelLength     := SelLength;
      vSaveSelAttrColor  := SelAttributes.Color;
      Lines.BeginUpdate;
      try
        SelectAll;
        SelAttributes.Assign(DefAttributes);
        SelAttributes.Name := Font.Name;

        MarkScriptSign(CGDM.CyclicParamSign);
        MarkScriptSign(CGDM.ConditionParamSign);
        MarkScriptSign(CGDM.ConstParamSign);
        MarkScriptSign(CGDM.SpacerSign);

      finally
        SelStart   := vSaveSelStart;
        SelLength  := vSaveSelLength;
        SelAttributes.Color := vSaveSelAttrColor;
        Lines.EndUpdate;
      end;//t..f

    finally
      OnChange := vSaveREOnChange;
    end;//t..f

  end;//with


  if Assigned(CGDM.MainForm) then
    TfrmCGMain(CGDM.MainForm).TemplateCodeFrameChanged(TemplateCode);
end;//TframeTemplateCode.MarkTemplateCode
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeTemplateCode.Create(AOwner: TComponent);
begin
  inherited;

  FParamsComplex := TParamsComplex.Create;

  with reTemplate do  begin
    DefAttributes.Name := Font.Name;
    DefAttributes.Size := Font.Size;

  end;//with
end;//TframeTemplateCode.Create
//==============================================================================
destructor TframeTemplateCode.Destroy;
begin
  FreeAndNil(FParamsComplex);

  inherited;
end;//TframeTemplateCode.Destroy
//==============================================================================
procedure TframeTemplateCode.LinkToMainForm;
// Назначить действия и показать фрейм
begin

  Show;
end;//TframeTemplateCode.LinkToMainForm
//==============================================================================
procedure TframeTemplateCode.UnLinkToMainForm;
// Сбросить действия и скрыть фрейм
begin

  Hide;
end;//TframeTemplateCode.UnLinkToMainForm
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeTemplateCode.reTemplateChange(Sender: TObject);
begin
  if Assigned(TemplateCode) then
    MarkTemplateCode;
end;//TframeTemplateCode.reTemplateChange
//==============================================================================
procedure TframeTemplateCode.reTemplateSelectionChange(Sender: TObject);
begin
  with (Sender as TRichEdit), CaretPos do  begin
    sbTemplate.Panels[0].Text := Format('%d:%d', [Y, X]);

  end;//with
end;//TframeTemplateCode.reTemplateSelectionChange
//==============================================================================
procedure TframeTemplateCode.reTemplateKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if (Key = VK_INSERT) and (Shift = []) then
    with sbTemplate.Panels[1] do
      if Text = 'Вставка' then
        Text := 'Замена'
      else
        Text := 'Вставка';

end;//TframeTemplateCode.reTemplateKeyDown
//==============================================================================
procedure TframeTemplateCode.sbtnCyclicParamClick(Sender: TObject);
begin
  AddScriptSign(CGDM.CyclicParamSign);
end;//TframeTemplateCode.sbtnCyclicParamClick
//==============================================================================
procedure TframeTemplateCode.sbtnConditionParamClick(Sender: TObject);
begin
  AddScriptSign(CGDM.ConditionParamSign);
end;//TframeTemplateCode.sbtnConditionParamClick
//==============================================================================
procedure TframeTemplateCode.sbtnConstParamClick(Sender: TObject);
begin
  AddScriptSign(CGDM.ConstParamSign);
end;//TframeTemplateCode.sbtnConstParamClick
//==============================================================================
procedure TframeTemplateCode.sbtnSpacerClick(Sender: TObject);
begin
  AddScriptSign(CGDM.SpacerSign);
end;//TframeTemplateCode.sbtnSpacerClick
//==============================================================================


END.
