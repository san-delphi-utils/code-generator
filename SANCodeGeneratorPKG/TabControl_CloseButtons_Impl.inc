//==============================================================================
// TTabControl
//==============================================================================
// private
//==============================================================================
procedure TTabControl.Set_CBIndex(const Value: integer);
begin
  if FCBIndex = Value then
    Exit;

  FCBIndex := Value;

  Invalidate;
end;//TTabControl.Set_CBIndex
//==============================================================================
//==============================================================================
// private, messages
//==============================================================================
procedure TTabControl.WMLButtonDown(var Message: TWMLButtonDown);
var
  vTabIndex : integer;
  vHandled : boolean;
begin
  vHandled := False;

  vTabIndex := IndexOfTabAt(Message.XPos, Message.YPos);

  if (vTabIndex > -1) and PtInRect(TabCloseButtonRect(vTabIndex, vTabIndex = TabIndex), Point(Message.XPos, Message.YPos)) then
    if AllowCloseButton(vTabIndex) then
      CloseButtonClick(vTabIndex, vHandled);


  if not vHandled then
    inherited;
end;//TTabControl.WMLButtonDown
//==============================================================================
// protected
//==============================================================================
procedure TTabControl.DrawTab(TabIndex: Integer; const Rect: TRect; Active: Boolean);
var
  vRect, vCBRect : TRect;
  vBMPInsex : integer;
begin
  inherited;

  vRect := Rect;

  with Canvas do  begin
    TextRect(vRect, vRect.Left + 10, vRect.Top + (RectHeight(vRect) - TextHeight('1')) div 2  + 1, Tabs[TabIndex]);

    vCBRect := TabCloseButtonRect(TabIndex, Active);

    vBMPInsex := ORD(not (PtInRect(vCBRect, ScreenToClient(Mouse.CursorPos)) and AllowCloseButton(TabIndex)) );

    CopyRect(vCBRect, FCloseButtonBMP.Canvas, Bounds(vBMPInsex * FCloseButtonBMP.Height, 0, FCloseButtonBMP.Height, FCloseButtonBMP.Height));
  end;//with
end;//TTabControl.DrawTab
//==============================================================================
procedure TTabControl.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  vTabIndex : integer;
begin
  vTabIndex := IndexOfTabAt(X, Y);

  if (vTabIndex > -1) and PtInRect(TabCloseButtonRect(vTabIndex, vTabIndex = TabIndex), Point(X, Y)) then
    CBIndex := vTabIndex
  else
    CBIndex := -1;

  inherited;
end;//TTabControl.MouseMove
//==============================================================================
procedure TTabControl.MouseLeave(Sender: TObject);
begin
  CBIndex := -1;
end;//TTabControl.MouseLeave
//==============================================================================
function TTabControl.AllowCloseButton(const ATabIndex: integer): boolean;
begin
  Result := True;

  if Assigned(FOnAllowCloseButton) then
    FOnAllowCloseButton(Self, ATabIndex, Result);
end;//TTabControl.AllowCloseButton
//==============================================================================
procedure TTabControl.CloseButtonClick(const ATabIndex: integer; var Handled: boolean);
var
  vAllow : boolean;
begin
  Handled := True;
  vAllow  := True;

  if Assigned(OnCloseButtonClick) then
    OnCloseButtonClick(Self, ATabIndex, vAllow, Handled);

  if vAllow then
    Tabs.Delete(ATabIndex);
end;//TTabControl.CloseButtonClick
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TTabControl.Create(AOwner: TComponent);
begin
  inherited;

  FCloseButtonBMP := TBitmap.Create;
  FCloseButtonBMP.LoadFromResourceName(GetModuleHandle('SANCodeGeneratorPKG.bpl'), 'SAN_CG_CLOSE_SMALL_RED_GRAY');

  OnMouseLeave := MouseLeave;
end;//TTabControl.Create
//==============================================================================
destructor TTabControl.Destroy;
begin
  FreeAndNil(FCloseButtonBMP);

  inherited;
end;//TTabControl.Destroy
//==============================================================================
function TTabControl.TabCloseButtonRect(const ATabIndex: integer; const AActive: Boolean): TRect;
begin

  if (ATabIndex < 0) or (ATabIndex > Tabs.Count - 1) then  begin
    Result := Rect(0, 0, -1, -1);
    Exit
  end;//if

  with TabRect(ATabIndex) do
    Result := Bounds(Right - FCloseButtonBMP.Height - 1 - 2*ORD(not AActive), Top + (RectHeight(TabRect(ATabIndex)) - FCloseButtonBMP.Height) div 2 + 1*ORD(not AActive), FCloseButtonBMP.Height, FCloseButtonBMP.Height);
end;//TTabControl.TabCloseButtonRect
//==============================================================================