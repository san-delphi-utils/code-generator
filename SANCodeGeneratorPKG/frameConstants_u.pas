UNIT frameConstants_u;//////////////////////////////////////////////////////////
// Фрейм отображения констант
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CGDM_u,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, frameCustomDrawGrid_u, ImgList, Menus, StdCtrls, Math, Types;
//==============================================================================
CONST
  DG_CONSTANTS_FIRST_ROW_COUNT  = 1;
  DG_CONSTANTS_FIRST_COL_COUNT  = 2;
//==============================================================================
TYPE
  TframeConstants = class(TframeCustomDrawGrid)
    mi_MoveUpRow: TMenuItem;
    mi_MoveDownRow: TMenuItem;
    mi_DeleteRow: TMenuItem;
    mi_InsertRow: TMenuItem;

    procedure FrameResize(Sender: TObject);
    procedure dgViewDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure dgViewRowMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure pmDGViewPopup(Sender: TObject);
    procedure mi_MoveUpRowClick(Sender: TObject);
    procedure mi_MoveDownRowClick(Sender: TObject);
    procedure mi_DeleteRowClick(Sender: TObject);
    procedure mi_InsertRowClick(Sender: TObject);

  private
    // Ссылка на обслуживаемый комплекс параметров
    FParamsComplex : TParamsComplex;

  private
    function Get_CGDM: TCGDM;

  protected
    function GridIsEmpty : boolean; override;

    procedure GetCellHint(const ACol, ARow : integer; var ACellHint : string); override;

    procedure GetCellEditer(const ACol, ARow : integer; var AEditerType : TFDGEditorType;
      var AFullEditerText, ASelEditerText : string); override;
    procedure SetCellEditerText(const ACol, ARow : integer; AEditerText : string); override;

    function CheckConstIsUsed(const AConstName : string) : boolean;

  public
    constructor Create(AOwner: TComponent); override;
    procedure UpdateGrid; override;

  public
    property CGDM : TCGDM  read Get_CGDM;

    property ParamsComplex : TParamsComplex  read FParamsComplex  write FParamsComplex;

  end;//TframeConstants
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  frmCGMain_u;
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
// TframeConstants
//==============================================================================
// private
//==============================================================================
function TframeConstants.Get_CGDM: TCGDM;
begin
  Result := TfrmCGMain(Owner).CGDM;
end;//TframeConstants.Get_CGDM
//==============================================================================
//==============================================================================
// protected
//==============================================================================
function TframeConstants.GridIsEmpty: boolean;
begin
  Result :=  not Assigned(FParamsComplex) or (ParamsComplex.Constants.Count = 0);
end;//TframeConstants.GridIsEmpty
//==============================================================================
procedure TframeConstants.GetCellHint(const ACol, ARow: integer; var ACellHint: string);
begin

  if (ACol < 1) or (ARow < 1) or GridIsEmpty then
    Exit;


  with FParamsComplex.Constants[ARow - DG_CONSTANTS_FIRST_ROW_COUNT] do
    if ACol = DG_CONSTANTS_FIRST_COL_COUNT then
      ACellHint := Text
    else
      ACellHint := Name;
end;//TframeConstants.GetCellHint
//==============================================================================
procedure TframeConstants.GetCellEditer(const ACol, ARow: integer;
  var AEditerType: TFDGEditorType; var AFullEditerText, ASelEditerText: string);
begin
  if ACol = 2 then
    begin
      AEditerType := fdgetMemo;
      AFullEditerText := FParamsComplex.Constants[ARow - DG_CONSTANTS_FIRST_ROW_COUNT].Text;
    end
  else
    begin
      AEditerType := fdgetEdit;
    end//else if
end;//TframeConstants.GetCellEditer
//==============================================================================
procedure TframeConstants.SetCellEditerText(const ACol, ARow: integer;  AEditerText: string);
begin
  if ACol = 2 then
    FParamsComplex.Constants[ARow - DG_CONSTANTS_FIRST_ROW_COUNT].Text := AEditerText

  else  with CGDM.ParamsComplex.Constants do  begin

    if ExistsByName(AEditerText) then
      AEditerText := GenUniqueName;

    Add(AEditerText).ParamKind := pkManual;

    UpdateGrid;
  end;//else

end;//TframeConstants.SetCellEditerText
//==============================================================================
function TframeConstants.CheckConstIsUsed(const AConstName: string): boolean;
// Проверить, используется ли константа хотябы одним шаблоном
var
  i : integer;
begin

  for i := 0 to CGDM.TemplateCodes.Count - 1 do
    if CGDM.TemplateCodes[i].ParamsComplex.Constants.ExistsByName(AConstName) then  begin
      Result := True;
      Exit;
    end;//if; for

  Result := False;
end;//TframeConstants.CheckConstIsUsed
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TframeConstants.Create(AOwner: TComponent);
begin
  inherited;

  with dgView do  begin
    dgView.ColWidths[0] := Canvas.TextWidth('0000');;
    dgView.ColWidths[1] := 150;

  end;//with
end;//TframeConstants.Create
//==============================================================================
procedure TframeConstants.UpdateGrid;
begin
  with dgView do  begin
    RowCount := MAX(FixedRows + 1, ParamsComplex.Constants.Count + DG_CONSTANTS_FIRST_ROW_COUNT);

    if GridIsEmpty then
      Options := Options - [goRowSizing, goRowMoving]
    else
      Options := Options + [goRowSizing, goRowMoving];

    Repaint;
  end;//with
end;//TframeConstants.UpdateGrid
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeConstants.FrameResize(Sender: TObject);
begin
  inherited;

  with dgView do
    ColWidths[2] := Width - 25 - ColWidths[0] - dgView.ColWidths[1] - GridLineWidth*2;

end;//TframeConstants.FrameResize
//==============================================================================
procedure TframeConstants.dgViewDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin

  with (Sender as TDrawGrid), Canvas, CenterPoint(Rect) do  begin


    if State * [gdSelected, gdFocused, gdFixed] = [] then
      FillRect(Rect);


    if (ACol in [0..2]) and (ARow = 0) then
      begin
        Font.Style := [fsBold];

        case ACol of
          0:
            DrawCellText(Rect, '#');

          1:
            DrawCellText(Rect, 'Константа');

          2:
            DrawCellText(Rect, 'Значение');

        end;//case
      end

    else  if (ACol = 0) then
      begin
        DrawCellText(Rect, IntToStr(ARow - DG_CONSTANTS_FIRST_ROW_COUNT + 1));
      end

    else
      with FParamsComplex.Constants[ARow - DG_CONSTANTS_FIRST_ROW_COUNT] do
        if (ACol = DG_CONSTANTS_FIRST_COL_COUNT) then
          begin
            Font.Color := CGDM.ConstParamSign.Color;
            DrawCellText(Rect, Text, '', taLeftJustify, 5, tlTop, 5);
          end
        else
          if Name = '' then
            begin
              Font.Color := clRed;
              DrawCellText(Rect, SCG_UNNAMED);
            end
          else
            begin
              Font.Color := CGDM.ConstParamSign.Color;
              DrawCellText(Rect, Name);
            end;//else if


  end;//with

end;//TframeConstants.dgViewDrawCell
//==============================================================================
procedure TframeConstants.dgViewRowMoved(Sender: TObject; FromIndex, ToIndex: Integer);
begin
  FParamsComplex.Constants.Move(FromIndex - DG_CONSTANTS_FIRST_ROW_COUNT, ToIndex - DG_CONSTANTS_FIRST_ROW_COUNT);
  (Sender as TDrawGrid).Repaint;
end;//TframeConstants.dgViewRowMoved
//==============================================================================
procedure TframeConstants.pmDGViewPopup(Sender: TObject);
begin
  mi_MoveUpRow.Visible    := (FParamsComplex.Constants.Count > 0);
  mi_MoveDownRow.Visible  := (FParamsComplex.Constants.Count > 0);
  mi_DeleteRow.Visible    := (FParamsComplex.Constants.Count > 0);

  mi_MoveUpRow.Enabled    := (dgView.Row - DG_CONSTANTS_FIRST_ROW_COUNT > 0);
  mi_MoveDownRow.Enabled  := (dgView.Row - DG_CONSTANTS_FIRST_ROW_COUNT < FParamsComplex.Constants.Count - 1);

  mi_DeleteRow.Enabled    := (FParamsComplex.Constants.Count > 0) and (not CheckConstIsUsed(FParamsComplex.Constants[dgView.Row - DG_CONSTANTS_FIRST_ROW_COUNT].Name));
end;//TframeConstants.pmDGViewPopup
//==============================================================================
procedure TframeConstants.mi_InsertRowClick(Sender: TObject);
begin
  dgView.Row := dgView.RowCount - 1;

  EditCell(1, dgView.RowCount - IfThen(GridIsEmpty, 1));
end;//TframeConstants.mi_InsertRowClick
//==============================================================================
procedure TframeConstants.mi_MoveUpRowClick(Sender: TObject);
begin
  with dgView do  begin
    FParamsComplex.Constants.Move(Row - DG_CONSTANTS_FIRST_ROW_COUNT, Row - DG_CONSTANTS_FIRST_ROW_COUNT - 1);
    Row := Row - 1;
    Repaint;
  end;//with
end;//TframeConstants.mi_MoveUpRowClick
//==============================================================================
procedure TframeConstants.mi_MoveDownRowClick(Sender: TObject);
begin
  with dgView do  begin
    FParamsComplex.Constants.Move(Row - DG_CONSTANTS_FIRST_ROW_COUNT, Row - DG_CONSTANTS_FIRST_ROW_COUNT + 1);
    Row := Row + 1;
    Repaint;
  end;//with
end;//TframeConstants.mi_MoveDownRowClick
//==============================================================================
procedure TframeConstants.mi_DeleteRowClick(Sender: TObject);
begin
  FParamsComplex.Constants.Delete(dgView.Row - DG_CONSTANTS_FIRST_ROW_COUNT);

  UpdateGrid;
end;//TframeConstants.mi_DeleteRowClick
//==============================================================================

END.
