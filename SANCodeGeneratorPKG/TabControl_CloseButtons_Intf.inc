//==============================================================================
  TTabControl = class;
//==============================================================================
  TTabControlTabIndexAllowEvent = procedure (Sender : TTabControl; const ATabIndex : integer; var Allow : boolean) of object;
  TTabControlTabIndexAllowHandledEvent = procedure (Sender : TTabControl; const ATabIndex : integer; var Allow, Handled : boolean) of object;
//==============================================================================
  TTabControl = class(ComCtrls.TTabControl)
  private
    FCloseButtonBMP : TBitmap;
    FCBIndex : integer;

    FOnAllowCloseButton : TTabControlTabIndexAllowEvent;
    FOnCloseButtonClick : TTabControlTabIndexAllowHandledEvent;

  private
    procedure Set_CBIndex(const Value: integer);

  private
    procedure WMLButtonDown(var Message: TWMLButtonDown); message WM_LBUTTONDOWN;

  protected
    procedure DrawTab(TabIndex: Integer; const Rect: TRect; Active: Boolean); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseLeave(Sender : TObject);
    function AllowCloseButton(const ATabIndex : integer) : boolean;
    procedure CloseButtonClick(const ATabIndex : integer; var Handled : boolean);

  protected
    property CBIndex : integer  read FCBIndex  write Set_CBIndex;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function TabCloseButtonRect(const ATabIndex : integer; const AActive: Boolean) : TRect;

  public
    property OnAllowCloseButton : TTabControlTabIndexAllowEvent    read FOnAllowCloseButton  write FOnAllowCloseButton;
    property OnCloseButtonClick : TTabControlTabIndexAllowHandledEvent  read FOnCloseButtonClick  write FOnCloseButtonClick;

  end;//TTabControl
//==============================================================================