UNIT frameConditionParamsDefs_u;////////////////////////////////////////////////
// Фрейм значений по умолчанию для условных параметров
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CGDM_u,
  Windows, Messages, SysUtils, Contnrs, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Math;
//==============================================================================
TYPE
  TframeConditionParamsDefs = class(TFrame)
    lbxCndParams: TListBox;
    mmDefaults: TMemo;
    pnlCndParams: TPanel;
    pnlDefaults: TPanel;
    spltrVrt: TSplitter;
    lblCndParam: TLabel;
    lblDefaults: TLabel;
    pnlClient: TPanel;
    procedure lbxCndParamsData(Control: TWinControl; Index: Integer; var Data: string);
    procedure lbxCndParamsClick(Sender: TObject);
    procedure mmDefaultsChange(Sender: TObject);

  private
    // Ссылка на обслуживаемый комплекс параметров
    FParamsComplex : TParamsComplex;

  private
    function Get_CurCndParam: TConditionParam;

  public
    procedure UpdateList;

  public
    property ParamsComplex : TParamsComplex  read FParamsComplex  write FParamsComplex;

    property CurCndParam : TConditionParam  read Get_CurCndParam;

  end;//TframeConditionParamsDefs
//==============================================================================
IMPLEMENTATION
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
// TframeConditionParamsDefs
//==============================================================================
// private
//==============================================================================
function TframeConditionParamsDefs.Get_CurCndParam: TConditionParam;
begin
  with lbxCndParams do
    if ItemIndex > -1 then
      Result := ParamsComplex.ConditionParams[ItemIndex]
    else
      Result := nil;
end;//TframeConditionParamsDefs.Get_CurCndParam
//==============================================================================
//==============================================================================
// public
//==============================================================================
procedure TframeConditionParamsDefs.UpdateList;
begin
  with lbxCndParams do  begin
    Count := ParamsComplex.ConditionParams.Count;

    ItemIndex := MIN(0, Count - 1);

    mmDefaults.Enabled := (Count > 0);
  end;//with
end;//TframeConditionParamsDefs.UpdateList
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeConditionParamsDefs.lbxCndParamsData(Control: TWinControl;
  Index: Integer; var Data: string);
begin
  Data := ParamsComplex.ConditionParams[Index].Name;

  if Data = '' then
    Data := '<Unnamed>';
end;//TframeConditionParamsDefs.lbxCndParamsData
//==============================================================================
procedure TframeConditionParamsDefs.lbxCndParamsClick(Sender: TObject);
var
  vMMDefOnChange : TNotifyEvent;
begin
  if (Sender as TListBox).ItemIndex = -1 then
    Exit;


  vMMDefOnChange := mmDefaults.OnChange;
  try
    mmDefaults.OnChange := nil;

    mmDefaults.Lines.Assign(CurCndParam.CyclicParamsDefs);

  finally
    mmDefaults.OnChange := vMMDefOnChange;
  end;//t..f
end;//TframeConditionParamsDefs.lbxCndParamsClick
//==============================================================================
procedure TframeConditionParamsDefs.mmDefaultsChange(Sender: TObject);
var
  i, j : integer;
begin
  with CurCndParam.CyclicParamsDefs do  begin
    Assign((Sender as TMemo).Lines);

    i := 0;
    while i <= Count - 1 do  begin

      for j := Count - 1 downto i + 1 do
        if Strings[i] = Strings[j] then
          Delete(j);

      Inc(i);
    end;//while

  end;//with
end;//TframeConditionParamsDefs.mmDefaultsChange
//==============================================================================


END.
