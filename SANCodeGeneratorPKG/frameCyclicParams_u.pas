UNIT frameCyclicParams_u;///////////////////////////////////////////////////////
// Фрейм отображения циклических параметров и соответствующих им условий
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  CGDM_u,
  Windows, Messages, SysUtils, Contnrs, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, Grids, {Важно: frameCustomDrawGrid_u после Grids} frameCustomDrawGrid_u,
  ImgList, Math, Types, StdCtrls;
//==============================================================================
CONST
  DG_CYCLIC_PARAMS_FIRST_ROW_COUNT  = 1;
  DG_CYCLIC_PARAMS_FIRST_COL_COUNT  = 2;
//==============================================================================
TYPE
  TframeCyclicParams = class(TframeCustomDrawGrid)
    mi_InsertRow: TMenuItem;
    mi_MoveUpRow: TMenuItem;
    mi_MoveDownRow: TMenuItem;
    mi_DelRow: TMenuItem;
    mi_DelCol: TMenuItem;

    procedure dgViewDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure pmDGViewPopup(Sender: TObject);
    procedure mi_InsertRowClick(Sender: TObject);
    procedure mi_MoveUpRowClick(Sender: TObject);
    procedure mi_MoveDownRowClick(Sender: TObject);
    procedure mi_DelRowClick(Sender: TObject);
    procedure dgViewRowMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure mi_DelColClick(Sender: TObject);

  private
    // Ссылка на обслуживаемый комплекс параметров
    FParamsComplex : TParamsComplex;

  private
    function Get_CGDM: TCGDM;

  protected
    function GridIsEmpty : boolean; override;

    procedure GetCellHint(const ACol, ARow : integer; var ACellHint : string); override;

    procedure GetCellEditer(const ACol, ARow : integer; var AEditerType : TFDGEditorType;
      var AFullEditerText, ASelEditerText : string); override;
    procedure SetCellEditerText(const ACol, ARow : integer; AEditerText : string); override;

    function CndParamIsUsed(const AConditionParam : TConditionParam) : boolean;

  public
    procedure UpdateGrid; override;

  public
    property CGDM : TCGDM  read Get_CGDM;

    property ParamsComplex : TParamsComplex  read FParamsComplex  write FParamsComplex;

  end;//TframeCyclicParams
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  frmCGMain_u;
//==============================================================================
{$R *.dfm}
//==============================================================================

//==============================================================================
// TframeCyclicParams
//==============================================================================
// private
//==============================================================================
function TframeCyclicParams.Get_CGDM: TCGDM;
begin
  Result := TfrmCGMain(Owner).CGDM;
end;//TframeCyclicParams.Get_CGDM
//==============================================================================
//==============================================================================
// protected
//==============================================================================
function TframeCyclicParams.GridIsEmpty: boolean;
begin
  Result := not Assigned(FParamsComplex) or (FParamsComplex.CyclicParams.Count = 0);
end;//TframeCyclicParams.GridIsEmpty
//==============================================================================
procedure TframeCyclicParams.GetCellHint(const ACol, ARow: integer; var ACellHint: string);
begin

  if (ACol < 1) or (ARow < 0) or (not Assigned(FParamsComplex)) then
    Exit;


  if (ARow = 0) then
    begin
      if ACol >= DG_CYCLIC_PARAMS_FIRST_COL_COUNT then
        ACellHint := FParamsComplex.ConditionParams[ACol - DG_CYCLIC_PARAMS_FIRST_COL_COUNT].Name
    end

  else  if not GridIsEmpty then
    if (ACol = 1) then
      begin
        ACellHint := FParamsComplex.CyclicParams[ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT].Text;
      end;

end;//TframeCyclicParams.GetCellHint
//==============================================================================
procedure TframeCyclicParams.GetCellEditer(const ACol, ARow: integer;
  var AEditerType: TFDGEditorType; var AFullEditerText, ASelEditerText: string);
var
  vCndParam : TConditionParam;
begin
  if ACol = 1 then
    begin
      AEditerType := fdgetEdit;
      AFullEditerText := FParamsComplex.CyclicParams[ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT].Text;
    end
  else
    begin
      vCndParam := FParamsComplex.ConditionParams[ACol - DG_CYCLIC_PARAMS_FIRST_COL_COUNT];

      ASelEditerText := FParamsComplex.CndByCyclVals[
        FParamsComplex.CyclicParams[ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT].ID,
        vCndParam.Name
      ];

      if vCndParam.CyclicParamsDefs.Count = 0 then
        begin
          AEditerType := fdgetEdit;
          AFullEditerText := ASelEditerText;
        end
      else
        begin
          AEditerType := fdgetComboBox;
          AFullEditerText := vCndParam.CyclicParamsDefs.Text;
        end;//else

    end;//else if

end;//TframeCyclicParams.GetCellEditer
//==============================================================================
procedure TframeCyclicParams.SetCellEditerText(const ACol, ARow: integer;
  AEditerText: string);
begin

  if ACol = 1 then
    FParamsComplex.CyclicParams[ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT].Text := AEditerText

  else
    FParamsComplex.CndByCyclVals[
      FParamsComplex.CyclicParams[ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT].ID,
      FParamsComplex.ConditionParams[ACol - DG_CYCLIC_PARAMS_FIRST_COL_COUNT].Name
    ] := AEditerText;

end;//TframeCyclicParams.SetCellEditerText
//==============================================================================
function TframeCyclicParams.CndParamIsUsed(const AConditionParam: TConditionParam): boolean;
var
  i : integer;
begin

  for i := 0 to CGDM.TemplateCodes.Count - 1 do
    if CGDM.TemplateCodes[i].ParamsComplex.ConditionParams.ExistsByName(AConditionParam.Name) then  begin
      Result := True;
      Exit;
    end;//if; fot

  Result := False;
end;//TframeCyclicParams.CndParamIsUsed
//==============================================================================
//==============================================================================
// public
//==============================================================================
procedure TframeCyclicParams.UpdateGrid;
begin
  with dgView do  begin

    // Новые размеры
    RowCount := MAX(2, ParamsComplex.CyclicParams.Count  + DG_CYCLIC_PARAMS_FIRST_ROW_COUNT);
    ColCount := ParamsComplex.ConditionParams.Count      + DG_CYCLIC_PARAMS_FIRST_COL_COUNT;

    ColWidths[0] := Canvas.TextWidth('0000');
    ColWidths[1] := 150;

    if GridIsEmpty then
      Options := Options - [goRowSizing, goRowMoving]
    else
      Options := Options + [goRowSizing, goRowMoving];

    Repaint;
  end;//with
end;//TframeCyclicParams.UpdateGrid
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TframeCyclicParams.dgViewDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin

  with (Sender as TDrawGrid), Canvas, CenterPoint(Rect) do  begin


    if State * [gdSelected, gdFocused, gdFixed] = [] then
      FillRect(Rect);


    if (ACol in [0, 1]) and (ARow = 0) then
      begin
        Font.Style := [fsBold];

        case ACol of
          0:
            DrawCellText(Rect, '#');

          1:
            DrawCellText(Rect, 'Параметр');

        end;//case
      end

    else  if (ARow = 0) then
      with FParamsComplex.ConditionParams[ACol - DG_CYCLIC_PARAMS_FIRST_COL_COUNT] do
        if Name = '' then
          begin
             Font.Color := clRed;
             DrawCellText(Rect, SCG_UNNAMED);
          end
        else
          begin
            Font.Color := CGDM.ConditionParamSign.Color;
            DrawCellText(Rect, Name);
          end

    else  if (ACol = 0) then
      begin
        DrawCellText(Rect, IntToStr(ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT + 1));
      end

    else  if (ACol = 1) then
      begin
        Font.Color := CGDM.CyclicParamSign.Color;

        DrawCellText(Rect, FParamsComplex.CyclicParams[ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT].Text);
      end

    else
      DrawCellText(
        Rect,

        FParamsComplex.CndByCyclVals[
          FParamsComplex.CyclicParams[ARow - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT].ID,
          FParamsComplex.ConditionParams[ACol - DG_CYCLIC_PARAMS_FIRST_COL_COUNT].Name
        ]
      );



  end;//with

end;//TframeCyclicParams.dgViewDrawCell
//==============================================================================
procedure TframeCyclicParams.dgViewRowMoved(Sender: TObject; FromIndex, ToIndex: Integer);
begin
  ParamsComplex.CyclicParams.Move(FromIndex - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT, ToIndex - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT);
  (Sender as TDrawGrid).Repaint;
end;//TframeCyclicParams.dgViewRowMoved
//==============================================================================
procedure TframeCyclicParams.pmDGViewPopup(Sender: TObject);
begin
  mi_InsertRow.Visible    := True;

  mi_DelRow.Visible       := (CGDM.ParamsComplex.CyclicParams.Count > 0);
  mi_MoveUpRow.Visible    := (CGDM.ParamsComplex.CyclicParams.Count > 0);
  mi_MoveDownRow.Visible  := (CGDM.ParamsComplex.CyclicParams.Count > 0);
  mi_DelCol.Visible       := (MDGridCoord.X >= DG_CYCLIC_PARAMS_FIRST_COL_COUNT)
                         and (CGDM.ParamsComplex.ConditionParams.Count > 0)
                         and (not GridIsEmpty or (MDGridCoord.Y = 0));

  mi_MoveUpRow.Enabled    := dgView.Row > 1;
  mi_MoveDownRow.Enabled  := dgView.Row < dgView.RowCount - 1;
  mi_DelCol.Enabled       := mi_DelCol.Visible and (not CndParamIsUsed(CGDM.ParamsComplex.ConditionParams[MDGridCoord.X - DG_CYCLIC_PARAMS_FIRST_COL_COUNT]));
end;//TframeCyclicParams.pmDGViewPopup
//==============================================================================
procedure TframeCyclicParams.mi_InsertRowClick(Sender: TObject);
var
  vCyclicParam : TCyclicParam;
  i : integer;
  vName : string;
begin
  vCyclicParam := TCyclicParam.Create(ParamsComplex.CyclicParams, ParamsComplex.CyclicParams.GenID);

  i := 1;
  repeat
    vName := Format('<CyclicParam%d>', [i]);

    if not ParamsComplex.CyclicParams.ExistsByText(vName) then
      Break;

    Inc(i);
  until FALSE;

  vCyclicParam.Text := vName;

  ParamsComplex.CyclicParams.Insert(dgView.Row - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT, vCyclicParam);

  UpdateGrid;
end;//TframeCyclicParams.mi_InsertRowClick
//==============================================================================
procedure TframeCyclicParams.mi_MoveUpRowClick(Sender: TObject);
begin
  ParamsComplex.CyclicParams.Move(dgView.Row - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT, dgView.Row - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT - 1);
  dgView.Row := dgView.Row - 1;

  dgView.Repaint;
end;//TframeCyclicParams.mi_MoveUpRowClick
//==============================================================================
procedure TframeCyclicParams.mi_MoveDownRowClick(Sender: TObject);
begin
  ParamsComplex.CyclicParams.Move(dgView.Row - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT, dgView.Row - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT + 1);
  dgView.Row := dgView.Row + 1;

  dgView.Repaint;
end;//TframeCyclicParams.mi_MoveDownRowClick
//==============================================================================
procedure TframeCyclicParams.mi_DelRowClick(Sender: TObject);
begin
  ParamsComplex.CyclicParams.Delete(dgView.Row - DG_CYCLIC_PARAMS_FIRST_ROW_COUNT);

  UpdateGrid;
end;//TframeCyclicParams.mi_DelRowClick
//==============================================================================
procedure TframeCyclicParams.mi_DelColClick(Sender: TObject);
begin
  ParamsComplex.ConditionParams.Delete(MDGridCoord.X - DG_CYCLIC_PARAMS_FIRST_COL_COUNT);

  UpdateGrid;
end;//TframeCyclicParams.mi_DelColClick
//==============================================================================

END.
