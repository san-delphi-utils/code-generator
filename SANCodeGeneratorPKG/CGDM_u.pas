UNIT CGDM_u;////////////////////////////////////////////////////////////////////
// Главный модуль данных генератора кода
INTERFACE///////////////////////////////////////////////////////////////////////
//==============================================================================
USES
  frmCGSplash_u,
  Windows, SysUtils, Classes, Contnrs, Controls, Forms, Graphics,
  SHFolder, Dialogs, Math, StrUtils, Variants, xmldom, XMLIntf, msxmldom, XMLDoc,
  TypInfo;
//==============================================================================
CONST
  // Путь к папке программы
  SAN_CODE_GENERATOR_FOLDER_PATH = 'SAN\CodeGenerator';

  // PARAM SIGNS
  SCG_CYCLIC_PARAM_SIGN     = 'CyclicParamSign';
  SCG_CONDITION_PARAM_SIGN  = 'ConditionParamSign';
  SCG_CONST_PARAM_SIGN      = 'ConstParamSign';
  SCG_SPACER_SIGN           = 'SpacerSign';

  // Безымянный элемент
  SCG_UNNAMED               = '<Unnamed>';

  // Генирировать ID
  SCG_GEN_ID                = Low(Integer);

  // Флаги для EnumStrFragments
  ESF_NOT_DUPLICATES_IN_LINE  = $0001;
  ESF_USE_FILTER_NAME         = $0002;
//==============================================================================
TYPE
  TParamTextSourceSeporationKind = (ptsskLine, ptsskWord, ptsskSubString);
  TParamKind = (pkAutoGen, pkManual);
//==============================================================================
  ECGCodeSource = class(Exception);
//==============================================================================
  TCGDM                 = class;
  TTemplateCodeList     = class;
  TScriptSign           = class;
  TScriptSignsList      = class;
  TCyclicParamsList     = class;
  TConditionParamsList  = class;
  TConstParamsList      = class;
  TParamsComplex        = class;
//==============================================================================
  // Интерфейс взаимодействия с библиотекой экспертов
  IIDEEXT_MainToolsDLG = interface(IUnknown)
    ['{3C669B59-19D6-420B-9DC6-3A9AC7A2AA0E}']

    function ShowMainToolsDLG : boolean;
  end;//IIDEEXT_MainToolsDLG
//==============================================================================
  ICG_Controller = interface(IUnknown)
    ['{92AC1CFB-E779-434B-866E-16B6FD7192F2}']

    function Get_OnInsertResult : TNotifyEvent;
    procedure Set_OnInsertResult(const AOnInsertResult : TNotifyEvent);
    property OnInsertResult : TNotifyEvent  read Get_OnInsertResult  write Set_OnInsertResult;

    procedure Show;

  end;//ICG_Controller
//==============================================================================
  TTemplateCode = class(TPersistent)
  private
    FOwnerList : TTemplateCodeList;

    FTemplateName : string;

    FFrame : TFrame;


  private
    function Get_Index: integer;
    function Get_TemplateText: TStrings;
    function Get_ParamsComplex: TParamsComplex;

  protected

  public
    constructor Create(AOwnerList : TTemplateCodeList; const ATemplateName : string);
    destructor Destroy; override;

    function LoadCode : boolean;
    function SaveCode : boolean;

  public
    property OwnerList : TTemplateCodeList  read FOwnerList;

    property Index : integer  read Get_Index;

    property TemplateName    : string    read FTemplateName;
    property TemplateText    : TStrings  read Get_TemplateText;

    property Frame : TFrame  read FFrame;

    property ParamsComplex : TParamsComplex  read Get_ParamsComplex;

  end;//TTemplateCode
//==============================================================================
  TTemplateCodeList = class(TObjectList)
  private
    FCGDM : TCGDM;

  private
    function GetItem(Index: Integer): TTemplateCode; inline;

  public
//    constructor Create; overload;
//    constructor Create(AOwnsObjects: Boolean); overload;

    function Extract(Item: TTemplateCode) : TTemplateCode; inline;
    function First: TTemplateCode; inline;
    function Last: TTemplateCode; inline;

    function Add(const ATemplateName : string) : TTemplateCode; inline;

    function Find(const ATemplateName : string) : TTemplateCode;

  public
    property Items[Index: Integer]: TTemplateCode read GetItem; default;

    property CGDM : TCGDM  read FCGDM;
  end;//TTemplateCodeList
//==============================================================================
  TScriptSignFiled = (ssfSignBeg, ssfSignEnd, ssfColor);
  TScriptSignFileds = set of TScriptSignFiled;
  TScriptSignChangeEvent = procedure(Sender : TScriptSign; const AUpdatedFilelds : TScriptSignFileds) of object;
  TScriptSignChangingEvent = procedure(Sender : TScriptSign; const AUpdatedFileld : TScriptSignFiled; var AAllow : boolean) of object;
//------------------------------------------------------------------------------
  TScriptSign = class(TPersistent)
  private
    FOwnerList : TScriptSignsList;

    FUpdatesCount : integer;
    FUpdatedFilelds : TScriptSignFileds;

    FName       : string;
    FIsRanged   : boolean;
    FSortOrder  : integer;

    FSignBeg, FSignEnd : string;
    FColor  : TColor;


    FOnChange : TScriptSignChangeEvent;
    FOnChanging : TScriptSignChangingEvent;

  private
    procedure Set_SignBeg(const Value: string);
    procedure Set_SignEnd(const Value: string);
    procedure Set_Color(const Value: TColor);

  protected
    procedure AssignTo(Dest : TPersistent); override;

    procedure Change(const AUpdatedFilelds : TScriptSignFileds);
    function AllowChanging(const AUpdatedFileld : TScriptSignFiled) : boolean;

    procedure ReadFromXML(const ANode : IXMLNode);
    procedure WriteToXML(const ANode : IXMLNode);


  public
    constructor Create(AOwnerList : TScriptSignsList; const AName : string;
      const AIsRanged : boolean; const ASortOrder : integer);
    destructor Destroy; override;

    procedure BeginUpdate;
    procedure EndUpdate(const ASilent : boolean = False);

  public
    property UpdatesCount : integer  read FUpdatesCount;

    property Name        : string   read FName;
    property IsRanged    : boolean  read FIsRanged  write FIsRanged;
    property SortOrder   : integer  read FSortOrder;

    property SignBeg   : string  read FSignBeg   write Set_SignBeg;
    property SignEnd   : string  read FSignEnd   write Set_SignEnd;
    property Color  : TColor  read FColor  write Set_Color;


    property OnChange    : TScriptSignChangeEvent    read FOnChange    write FOnChange;
    property OnChanging  : TScriptSignChangingEvent  read FOnChanging  write FOnChanging;
  end;//TScriptSign
//==============================================================================
  TScriptSignsList = class(TObjectList)
  private
    function GetItem(Index: Integer): TScriptSign; inline;

  public
    function Extract(Item: TScriptSign): TScriptSign; inline;
    function First: TScriptSign; inline;
    function Last: TScriptSign; inline;

    function FindAndGetByName(const AName : string; var AScriptSign : TScriptSign; AExclude : TScriptSign = nil) : boolean;
    function FindByName(const AName : string; AExclude : TScriptSign = nil) : TScriptSign;
    function ExistsByName(const AName : string; AExclude : TScriptSign = nil) : boolean;

    function Add(const AName : string; const AIsNumbered  : boolean; const ASortOrder : integer): TScriptSign; inline;

    procedure ReadFromXML(const ANode : IXMLNode);
    procedure WriteToXML(const ANode : IXMLNode);

  public
    property Items[Index: Integer]: TScriptSign  read GetItem; default;
  end;//TScriptSignsList
//==============================================================================
  TCyclicParam = class(TPersistent)
  private
    FOwnerList : TCyclicParamsList;

    FID : integer;

    FText : string;

  protected
    procedure AssignTo(Dest : TPersistent); override;

  public
    constructor Create(AOwnerList : TCyclicParamsList; const AID : integer);
    destructor Destroy; override;

  public
    property ID : integer  read FID;
    property Text : string  read FText  write FText;
  end;//TCyclicParam
//==============================================================================
  TCyclicParamsList = class(TObjectList)
  private
    FIDCounter : integer;

  private
    function GetItem(Index: Integer): TCyclicParam; inline;

  public
    function Extract(Item: TCyclicParam): TCyclicParam; inline;
    function First: TCyclicParam; inline;
    function Last: TCyclicParam; inline;

    function GenID : integer;

    function FindAndGetByID(const AID : integer; var ACyclicParam : TCyclicParam; AExclude : TCyclicParam = nil) : boolean;
    function FindByID(const AID : integer; AExclude : TCyclicParam = nil) : TCyclicParam;
    function ExistsByID(const AID : integer; AExclude : TCyclicParam = nil) : boolean;

    function FindAndGetByText(const AText : string; var ACyclicParam : TCyclicParam; AExclude : TCyclicParam = nil) : boolean;
    function FindByText(const AText : string; AExclude : TCyclicParam = nil) : TCyclicParam;
    function ExistsByText(const AText : string; AExclude : TCyclicParam = nil) : boolean;

    function Add(const AText : string; AID : integer = SCG_GEN_ID): TCyclicParam; inline;
    function TryAdd(const AText : string; const AID : integer = SCG_GEN_ID): TCyclicParam;

    function AddCyclicParams(ACyclicParams : TCyclicParamsList) : integer;
    function DeleteOtherCyclicParams(ACyclicParams : TCyclicParamsList) : integer;

  public
    property Items[Index: Integer]: TCyclicParam  read GetItem; default;

    property IDCounter : integer  read FIDCounter  write FIDCounter;
  end;//TCyclicParamsList
//==============================================================================
  TConditionParam = class(TPersistent)
  private
    FOwnerList : TConditionParamsList;

    FName : string;

    FCyclicParamsDefs : TStrings;


  private

  protected
    procedure AssignTo(Dest : TPersistent); override;

  public
    constructor Create(AOwnerList : TConditionParamsList; const AName : string);
    destructor Destroy; override;

  public
    property Name : string  read FName;

    property CyclicParamsDefs : TStrings  read FCyclicParamsDefs;
  end;//TConditionParam
//==============================================================================
  TConditionParamsList = class(TObjectList)
  private
    function GetItem(Index: Integer): TConditionParam; inline;

  public
    function Extract(Item: TConditionParam): TConditionParam; inline;
    function First: TConditionParam; inline;
    function Last: TConditionParam; inline;

    function FindAndGetByName(const AName : string; var AConditionParam : TConditionParam; AExclude : TConditionParam = nil) : boolean;
    function FindByName(const AName : string; AExclude : TConditionParam = nil) : TConditionParam;
    function ExistsByName(const AName : string; AExclude : TConditionParam = nil) : boolean;

    function Add(const AName : string): TConditionParam; inline;
    function TryAdd(const AName : string): TConditionParam;

    function AddConditionParams(AConditionParams : TConditionParamsList) : integer;
    function DeleteOtherConditionParams(AConditionParams : TConditionParamsList) : integer;

  public
    property Items[Index: Integer]: TConditionParam  read GetItem; default;
  end;//TConditionParamsList
//==============================================================================
  PCndByCyclValRec = ^TCndByCyclValRec;
  TCndByCyclValRec = record
    CyclicParamID : integer;
    CndParamName, Value : string;
  end;//TCndByCyclValRec
//==============================================================================
  TCndByCyclValList = class(TList)
  private
    function Get_Item(const Index: integer): PCndByCyclValRec;

  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;

  public
    function Extract(Item: PCndByCyclValRec): PCndByCyclValRec; inline;
    function First: PCndByCyclValRec; inline;
    function Last: PCndByCyclValRec; inline;

    function FindAndGet(const ACyclicParamID : integer; const ACndParamName : string; var AItem : PCndByCyclValRec) : boolean;
    function Find(const ACyclicParamID : integer; const ACndParamName : string) : PCndByCyclValRec;
    function Exists(const ACyclicParamID : integer; const ACndParamName : string) : boolean;

    function Add(const ACyclicParamID : integer; const ACndParamName : string;
      const AValue : string = '') : PCndByCyclValRec; inline;

  public
    property Items[const Index : integer] : PCndByCyclValRec  read Get_Item; default;
  end;//TCndByCyclValList
//==============================================================================
  TConstParam = class(TPersistent)
  private
    FOwnerList : TConstParamsList;

    FName, FText : string;

    FParamKind : TParamKind;

  protected
    procedure AssignTo(Dest : TPersistent); override;

  public
    constructor Create(AOwnerList : TConstParamsList; const AName : string);
    destructor Destroy; override;

  public
    property Name : string  read FName;
    property Text : string  read FText  write FText;

    property ParamKind : TParamKind  read FParamKind  write FParamKind;

  end;//TConstParam
//==============================================================================
  TConstParamsList = class(TObjectList)
  private
    function GetItem(Index: Integer) : TConstParam; inline;

  public
    function Extract(Item: TConstParam) : TConstParam; inline;
    function First: TConstParam; inline;
    function Last: TConstParam; inline;

    function FindAndGetByName(const AName : string; var AConstParam : TConstParam; AExclude : TConstParam = nil) : boolean;
    function FindByName(const AName : string; AExclude : TConstParam = nil) : TConstParam;
    function ExistsByName(const AName : string; AExclude : TConstParam = nil) : boolean;

    function GenUniqueName : string;

    function Add(const AName : string): TConstParam; inline;
    function TryAdd(const AName : string): TConstParam;

    function AddConstParams(AConstParams : TConstParamsList) : integer;
    function DeleteOtherConstParams(AConstParams : TConstParamsList) : integer;

  public
    property Items[Index: Integer]: TConstParam  read GetItem; default;
  end;//TConstParamsList
//==============================================================================
  TParamsComplex = class(TPersistent)
  private
    FCyclicParams     : TCyclicParamsList;
    FConditionParams  : TConditionParamsList;
    FCndByCyclVals    : TCndByCyclValList;
    FConstants        : TConstParamsList;

  private
    function Get_CndByCyclVals(const ACyclicParamID: integer;
      const ACndParamName: string): string;
    procedure Set_CndByCyclVals(const ACyclicParamID: integer;
      const ACndParamName, Value: string);

  public
    constructor Create;
    destructor Destroy; override;

    procedure Clear;

    procedure RegisterParam(const ASign : TScriptSign; const AParamName : string);

    function AddParams(AParamsComplex : TParamsComplex) : integer;
    function DeleteOtherParams(AParamsComplex : TParamsComplex; const ACyclicParamsFlag : boolean = False) : integer;

    procedure ClearGarbage;

  public
    property CyclicParams     : TCyclicParamsList     read FCyclicParams;
    property ConditionParams  : TConditionParamsList  read FConditionParams;
    property Constants        : TConstParamsList      read FConstants;

    property CndByCyclVals [const ACyclicParamID : integer; const ACndParamName : string] : string
      read Get_CndByCyclVals  write Set_CndByCyclVals;

  end;//TParamsComplex
//==============================================================================
  TCGDM = class(TDataModule, IIDEEXT_MainToolsDLG, ICG_Controller)
    dlgOpenTemplate: TOpenDialog;
    dlgSaveTemplate: TSaveDialog;
    xmlConfig: TXMLDocument;

    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  private
    FRunModeAsApp : boolean;

    FSplashForm : TfrmCGSplash;

    FMainForm : TForm;

    FTemplateCodeList : TTemplateCodeList;

    FScriptSigns : TScriptSignsList;

    FParamsComplex : TParamsComplex;

    FRememberTemplateCodes,
    FParamsTextSourceAutoParseMode,
    FParamsDeleteNonExistsCyclic,
    FParamsDeleteNonExistsCondition,
    FParamsDeleteNonExistsConsts,
    FParamsTrimAll : boolean;


    FFramesUniqueGen : integer;

    FXMLNode, FParamsNode : IXMLNode;

    FParamTextSourceSeporationKind : TParamTextSourceSeporationKind;

    FParamTextSourceSepSubStr, FParamTextSourceSepWordsEndChars : string;

    FOnInsertResult : TNotifyEvent;

  private
    function Get_InstanceHandle: THandle;

    procedure Set_RememberTemplateCodes(const Value: boolean);

    function Get_CyclicParamSign: TScriptSign;
    function Get_ConditionParamSign: TScriptSign;
    function Get_ConstParamSign: TScriptSign;
    function Get_SpacerSign: TScriptSign;

    procedure Set_ParamsTextSourceAutoParseMode(const Value: boolean);
    procedure Set_ParamTextSourceSeporationKind(
      const Value: TParamTextSourceSeporationKind);
    procedure Set_ParamsDeleteNonExistsCondition(const Value: boolean);
    procedure Set_ParamsDeleteNonExistsConsts(const Value: boolean);
    procedure Set_ParamsDeleteNonExistsCyclic(const Value: boolean);
    procedure Set_ParamTextSourceSepSubStr(const Value: string);
    procedure Set_ParamsTrimAll(const Value: boolean);
    procedure Set_ParamTextSourceSepWordsEndChars(const Value: string);

  protected
    // IIDEEXT_MainToolsDLG
    function ShowMainToolsDLG : boolean;

    // ICG_Controller
    function Get_OnInsertResult : TNotifyEvent;
    procedure Set_OnInsertResult(const AOnInsertResult : TNotifyEvent);
    procedure Show;

    function FindUnLinkedTemplateFrame(var AFrame : TFrame) : boolean;
    function CreateNewTemplateFrame : TFrame;

    function GetXMLBoolAttr(ANode : IXMLNode; const AAttrName : string; const ADefault : boolean = False) : boolean;
    procedure SetXMLBoolAttr(ANode : IXMLNode; const AAttrName : string; const AValue : boolean);

  public
    class function AppDataFolder(const AForceDirectories : boolean = False) : string;
    class function AppDataTemplatesFolder(const AForceDirectories : boolean = False) : string;

    procedure CreateSplash;
    procedure DestroySplash;

    procedure ReCreateMainForm(const AShowNow : boolean);

    function OpenTemplateCode(var ATemplateCode : TTemplateCode) : integer;
    function SaveTemplateCode(const ATemplateCode : TTemplateCode) : boolean;
    function SaveResult(const AResultStrings : TStrings) : boolean;

    function CallToolsDLG : boolean;

    procedure ParamsAddFromTemplateCode(const ATemplateCode : TTemplateCode);

    procedure ParseParamsTextSource;

    procedure GenerateFor(const ATemplateCode : TTemplateCode; const AResultStrings : TStrings);

  public
    property RunModeAsApp : boolean  read FRunModeAsApp;

    property InstanceHandle : THandle  read Get_InstanceHandle;

    property MainForm : TForm  read FMainForm;

    property TemplateCodes : TTemplateCodeList  read FTemplateCodeList;
    property RememberTemplateCodes : boolean  read FRememberTemplateCodes  write Set_RememberTemplateCodes;


    property ScriptSigns : TScriptSignsList  read FScriptSigns;

    property CyclicParamSign     : TScriptSign  read Get_CyclicParamSign;
    property ConditionParamSign  : TScriptSign  read Get_ConditionParamSign;
    property ConstParamSign      : TScriptSign  read Get_ConstParamSign;
    property SpacerSign          : TScriptSign  read Get_SpacerSign;


    property ParamsComplex : TParamsComplex  read FParamsComplex;


    property ParamTextSourceSeporationKind   : TParamTextSourceSeporationKind  read FParamTextSourceSeporationKind  write Set_ParamTextSourceSeporationKind;

    property ParamsTextSourceAutoParseMode   : boolean  read FParamsTextSourceAutoParseMode   write Set_ParamsTextSourceAutoParseMode;
    property ParamsDeleteNonExistsCyclic     : boolean  read FParamsDeleteNonExistsCyclic     write Set_ParamsDeleteNonExistsCyclic;
    property ParamsDeleteNonExistsCondition  : boolean  read FParamsDeleteNonExistsCondition  write Set_ParamsDeleteNonExistsCondition;
    property ParamsDeleteNonExistsConsts     : boolean  read FParamsDeleteNonExistsConsts     write Set_ParamsDeleteNonExistsConsts;
    property ParamsTrimAll                   : boolean  read FParamsTrimAll                   write Set_ParamsTrimAll;

    property ParamTextSourceSepSubStr : string         read FParamTextSourceSepSubStr         write Set_ParamTextSourceSepSubStr;
    property ParamTextSourceSepWordsEndChars : string  read FParamTextSourceSepWordsEndChars  write Set_ParamTextSourceSepWordsEndChars;

    property OnInsertResult : TNotifyEvent  read FOnInsertResult;

  end;//TCGDM
//==============================================================================
function CreateSANCodeGeneratorAsApp : TCGDM;
function CreateSANCodeGeneratorAsExtEditer(AOwner : TComponent; const AOnInsertResult : TNotifyEvent) : TCGDM;
//==============================================================================
function EnumStrFragments(const AFragmentBeg, AFragmentEnd : string;
  ASirings : TStrings; AEnumProc : Pointer; AParam : Pointer; const AFlags : integer = 0;
  const AFirstLine : integer = 0; const AFirstCol : integer = 1;
  const AFragmentFilter : string = '') : integer;
//==============================================================================
IMPLEMENTATION//////////////////////////////////////////////////////////////////
//==============================================================================
uses
  frameTemplateCode_u, frmCGMain_u, frmToolsDLG_u;
//==============================================================================
{$R *.dfm}
//==============================================================================

type
  TfrmCGMainHack          = class(TfrmCGMain);
  TframeTemplateCodeHack  = class(TframeTemplateCode);

//==============================================================================
function CreateSANCodeGeneratorAsApp : TCGDM;
begin
  Application.CreateForm(TCGDM, Result);
  Result.FRunModeAsApp := True;

  Result.ReCreateMainForm(False);
end;//CreateSANCodeGeneratorAsApp
//==============================================================================
function CreateSANCodeGeneratorAsExtEditer(AOwner : TComponent;
  const AOnInsertResult : TNotifyEvent) : TCGDM;
begin
  Result := TCGDM.Create(AOwner);
  Result.FRunModeAsApp := False;
  Result.FOnInsertResult := AOnInsertResult;

  Result.ReCreateMainForm(False);
end;//CreateSANCodeGeneratorAsExtEditer
//==============================================================================

//==============================================================================
function GetSpecialFolderPath(Folder : integer) : string;
const
  SHGFP_TYPE_CURRENT = 0;
var
  path: array [0..MAX_PATH] of char;
begin
  if SUCCEEDED(SHGetFolderPath(0,Folder,0,SHGFP_TYPE_CURRENT,@path[0])) then
    Result := path
  else
    Result := '';
end;//GetSpecialFolderPath
//==============================================================================
procedure String_ExtractWords(S : string; AWordsList : TStrings;
  const ANotWordChars : string = ' .,;:');
// Заполнить список строк AWordsList словами из строки S
// Слово - участок текста состоящий из букв, цифр или знака припенения
// Знаки из строки ANotWordChars заканчивают слово
var
  i, j : integer;
begin
  AWordsList.Clear;

  S := Trim(S);

  while Length(S) > 0 do  begin
    i := 1;

    while i <= Length(S) do
      try

        for j := 1 to Length(ANotWordChars) do
          if S[i] = ANotWordChars[j] then
            Abort;

        Inc(i)
      except
        on EAbort do
          Break
        else
          raise
      end;//t..e

    AWordsList.Add( Copy(S, 1, i) );

    Delete(S, 1, i);
  end;//while

end;//String_ExtractWords
//==============================================================================
function EnumStrFragments(const AFragmentBeg, AFragmentEnd : string;
  ASirings : TStrings; AEnumProc : Pointer; AParam : Pointer; const AFlags : integer = 0;
  const AFirstLine : integer = 0; const AFirstCol : integer = 1;
  const AFragmentFilter : string = '') : integer;
// Найти в списке ASirings все подстроки ограчиченные AFragmentBeg, AFragmentEnd.
// Если AFragmentEnd, то ищется каждая пустая строка после AFragmentBeg
// Если задан AFragmentFilter - берутся, только подстроки с таким именем.
// Поиск начинается со строки AFirstLine и столбца AFirstCol этой строки.
// Если задана процедура AEnumProc, то она вызывается для каждого найденного
// фрагмента с параметром AParam.
// Возвращает общее число найденых фрагментов.
type
  TEnumFragmentsProc = procedure(const AFragmentBeg, AFragmentEnd : string;
    AStrings: TStrings; var ALine, ACol, AOffset : integer;
    const AFragmentName: string; var AComplete : boolean; AParam: Pointer);
//------------------------------------------------------------------------------
var
  l, vOffset, vIncOffset, vPosBeg, vPosEnd : integer;
  vFragmentName : string;
  vFragmentsInLine : TStringList;
  vEnumFragmentsProc : TEnumFragmentsProc;
  vComplete : boolean;
begin
  Result := 0;
  vComplete := False;

  vFragmentsInLine := TStringList.Create;
  try
    // Цикл по всему тексту, для поиска в каждой строке нового выр. пробела
    l := AFirstLine;
    while l <= ASirings.Count - 1 do  begin

      vOffset := IfThen(l = AFirstLine, AFirstCol, 1);
      vFragmentsInLine.Clear;

      repeat

        // Начало фрагмента
        vPosBeg := PosEx(AFragmentBeg, ASirings[l], vOffset);

        if vPosBeg = 0 then
          Break;


        // Конец фрагмента
        if AFragmentEnd <> '' then
          begin

            vPosEnd := PosEx(AFragmentEnd, ASirings[l], vPosBeg + Length(AFragmentBeg));
            if vPosEnd = 0 then
              raise ECGCodeSource.CreateFmt('В линии %d в столбце %d не закрытый'#13#10'фрагмент текста. Нет подстроки «%s»!', [l, vPosBeg, AFragmentEnd]);


            vFragmentName := Copy(ASirings[l], vPosBeg + Length(AFragmentBeg), vPosEnd - (vPosBeg + Length(AFragmentBeg)) );
          end
        else
          vFragmentName := '';


        vIncOffset := (vPosBeg + Length(AFragmentBeg) + Length(vFragmentName) + Length(AFragmentEnd)) - vOffset;


        if (AFlags and ESF_USE_FILTER_NAME = 0) or (vFragmentName = AFragmentFilter) then  begin


          if (AFlags and ESF_NOT_DUPLICATES_IN_LINE > 0) and (vFragmentsInLine.IndexOf(vFragmentName) > -1) then
            raise ECGCodeSource.CreateFmt('В линии %d в столбце %d более одного'#13#10'фрагмента с именем'#13#10'«%s»!', [l, vPosBeg, vFragmentName]);

          vFragmentsInLine.Add(vFragmentName);


          Inc(Result);


          if Assigned(AEnumProc) then  begin
            @vEnumFragmentsProc := AEnumProc;
            vEnumFragmentsProc(AFragmentBeg, AFragmentEnd, ASirings, l, vPosBeg, vIncOffset, vFragmentName, vComplete, AParam);

            if vComplete then
              Exit;

          end;//if

        end;//if

        vOffset := MAX(1, vOffset + vIncOffset);
      until FALSE;

      Inc(l);
    end;//while

  finally
    FreeAndNil(vFragmentsInLine);
  end;//t..f
end;//TCGDM.EnumSpacers
//==============================================================================

//==============================================================================
// TTemplateCode
//==============================================================================
// private
//==============================================================================
function TTemplateCode.Get_Index: integer;
begin
  Result := FOwnerList.IndexOf(Self);
end;//TTemplateCode.Get_Index
//==============================================================================
function TTemplateCode.Get_TemplateText: TStrings;
begin
  Result := TframeTemplateCode(FFrame).reTemplate.Lines;
end;//TTemplateCode.Get_TemplateText
//==============================================================================
function TTemplateCode.Get_ParamsComplex: TParamsComplex;
begin
  Result := TframeTemplateCode(FFrame).ParamsComplex;
end;//TTemplateCode.Get_ParamsComplex
//==============================================================================
//==============================================================================
// protected
//==============================================================================
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TTemplateCode.Create(AOwnerList : TTemplateCodeList; const ATemplateName: string);
begin
  inherited Create;

  FOwnerList := AOwnerList;

  FTemplateName := ATemplateName;

  FFrame := FOwnerList.FCGDM.CreateNewTemplateFrame;

  TframeTemplateCodeHack(FFrame).LinkToTemplateCode(Self);
end;//TTemplateCode.Create
//==============================================================================
destructor TTemplateCode.Destroy;
begin
  FOwnerList.Extract(Self);

  // Фрейм будет освобожден  при разрушении модуля данных
  FFrame.Parent := nil;
  TframeTemplateCodeHack(FFrame).LinkToTemplateCode(nil);
  FFrame := nil;

  inherited;
end;//TTemplateCode.Destroy
//==============================================================================
function TTemplateCode.LoadCode: boolean;
begin
  Result := FileExists(TemplateName);

  if Result then
    with TemplateText do  begin
      BeginUpdate;
      try
        LoadFromFile(TemplateName);
      finally
        EndUpdate;
      end;//t..f
    end;//with;if
end;//TTemplateCode.LoadCode
//==============================================================================
function TTemplateCode.SaveCode: boolean;
begin
  Result := DirectoryExists(ExtractFilePath(TemplateName));

  if Result then
    with TStringList.Create do  try
      Assign(TemplateText);

      SaveToFile(TemplateName);
    finally
      Free;
    end;//t..f; with; if
end;//TTemplateCode.SaveCode
//==============================================================================

//==============================================================================
// TTemplateCodeList
//==============================================================================
// private
//==============================================================================
function TTemplateCodeList.GetItem(Index: Integer): TTemplateCode;
begin
  Result := TTemplateCode(inherited Items[Index]);
end;//TTemplateCodeList.GetItem
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TTemplateCodeList.Extract(Item: TTemplateCode): TTemplateCode;
begin
  Result := TTemplateCode(inherited Extract(Item));
end;//TTemplateCodeList.Extract
//==============================================================================
function TTemplateCodeList.First: TTemplateCode;
begin
  Result := TTemplateCode(inherited First);
end;//TTemplateCodeList.First
//==============================================================================
function TTemplateCodeList.Last: TTemplateCode;
begin
  Result := TTemplateCode(inherited Last);
end;//TTemplateCodeList.Last
//==============================================================================
function TTemplateCodeList.Add(const ATemplateName: string): TTemplateCode;
begin
  Result := TTemplateCode.Create(Self, ATemplateName);

  inherited Add(Result);
end;//TTemplateCodeList.Add
//==============================================================================
function TTemplateCodeList.Find(const ATemplateName: string): TTemplateCode;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if Items[i].TemplateName = ATemplateName then  begin
      Result := Items[i];
      Exit;
    end;//if; for

  Result := nil;
end;//TTemplateCodeList.Find
//==============================================================================

//==============================================================================
// TScriptSign
//==============================================================================
// private
//==============================================================================
procedure TScriptSign.Set_SignBeg(const Value: string);
begin
  if (FSignBeg = Value) or (not AllowChanging(ssfSignBeg)) then
    Exit;

  FSignBeg := Value;

  Change([ssfSignBeg]);
end;//TScriptSign.Set_SignBeg
//==============================================================================
procedure TScriptSign.Set_SignEnd(const Value: string);
begin
  if (FSignEnd = Value) or (not AllowChanging(ssfSignEnd)) then
    Exit;

  FSignEnd := Value;

  Change([ssfSignEnd]);
end;//TScriptSign.Set_Sign
//==============================================================================
procedure TScriptSign.Set_Color(const Value: TColor);
begin
  if (FColor = Value) or (not AllowChanging(ssfColor)) then
    Exit;

  FColor := Value;

  Change([ssfColor]);
end;//TScriptSign.Set_Color
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TScriptSign.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TScriptSign)  then
    inherited

  else  with Dest as TScriptSign do  begin
    SignBeg  := Self.SignBeg;
    SignEnd  := Self.SignEnd;
    Color    := Self.Color;

  end;//with; else if

end;//TScriptSign.AssignTo
//==============================================================================
procedure TScriptSign.Change(const AUpdatedFilelds : TScriptSignFileds);
// Произошли изменения
begin
  if FUpdatesCount > 0 then
    FUpdatedFilelds := FUpdatedFilelds + AUpdatedFilelds
  else
    if Assigned(FOnChange) then
      FOnChange(Self, AUpdatedFilelds);
end;//TScriptSign.Change
//==============================================================================
function TScriptSign.AllowChanging(const AUpdatedFileld: TScriptSignFiled): boolean;
begin
  Result := True;

  if Assigned(FOnChanging) then
    FOnChanging(Self, AUpdatedFileld, Result);
end;//TScriptSign.AllowChanging
//==============================================================================
procedure TScriptSign.ReadFromXML(const ANode: IXMLNode);
begin
  BeginUpdate;
  try

    if ANode.Attributes['SignBeg'] <> Null then
      SignBeg := ANode.Attributes['SignBeg'];

    if ANode.Attributes['SignEnd'] <> Null then
      SignEnd := ANode.Attributes['SignEnd'];

    Color  := StringToColor(ANode.Attributes['Color']);

  finally
    EndUpdate;
  end;//t..f
end;//TScriptSign.ReadFromXML
//==============================================================================
procedure TScriptSign.WriteToXML(const ANode: IXMLNode);
begin
  ANode.Attributes['SignBeg']  := SignBeg;
  ANode.Attributes['SignEnd']  := SignEnd;
  ANode.Attributes['Color']    := ColorToString(Color)
end;//TScriptSign.WriteToXML
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TScriptSign.Create(AOwnerList: TScriptSignsList;
  const AName: string; const AIsRanged: boolean; const ASortOrder: integer);
begin
  inherited Create;

  FOwnerList := AOwnerList;

  FName := AName;

  FIsRanged := AIsRanged;

  FSortOrder := ASortOrder;
end;//TScriptSign.Create
//==============================================================================
destructor TScriptSign.Destroy;
begin
  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);

  inherited;
end;//TScriptSign.Destroy
//==============================================================================
procedure TScriptSign.BeginUpdate;
begin
  Inc(FUpdatesCount);
end;//TScriptSign.BeginUpdate
//==============================================================================
procedure TScriptSign.EndUpdate(const ASilent: boolean);
begin
  if FUpdatesCount = 0 then
    Exit;

  Dec(FUpdatesCount);

  if FUpdatesCount > 0 then
    Exit;

  if (not ASilent) then
    Change(FUpdatedFilelds);

  FUpdatedFilelds := [];
end;//TScriptSign.EndUpdate
//==============================================================================

//==============================================================================
// TScriptSignsList
//==============================================================================
// private
//==============================================================================
function TScriptSignsList.GetItem(Index: Integer): TScriptSign;
begin
  Result := TScriptSign(inherited Items[Index]);
end;//TScriptSignsList.GetItem
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TScriptSignsList.Extract(Item: TScriptSign): TScriptSign;
begin
  Result := TScriptSign(inherited Extract(Item));

  if Assigned(Item) and (Item.FOwnerList = Self)  then
    Item.FOwnerList := nil;
end;//TScriptSignsList.Extract
//==============================================================================
function TScriptSignsList.First: TScriptSign;
begin
  Result := TScriptSign(inherited First);
end;//TScriptSignsList.First
//==============================================================================
function TScriptSignsList.Last: TScriptSign;
begin
  Result := TScriptSign(inherited Last);
end;//TScriptSignsList.Last
//==============================================================================
function TScriptSignsList.FindAndGetByName(const AName: string;
  var AScriptSign: TScriptSign; AExclude : TScriptSign): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if  (Items[i] <> AExclude)
    and (Items[i].Name = AName)
    then  begin
      AScriptSign := Items[i];
      Result := True;
      Exit;
    end;//if; for

  Result := False;
end;//TScriptSignsList.FindAndGetByName
//==============================================================================
function TScriptSignsList.FindByName(const AName: string; AExclude : TScriptSign): TScriptSign;
begin
  Result := nil;
  FindAndGetByName(AName, Result, AExclude);
end;//TScriptSignsList.FindByName
//==============================================================================
function TScriptSignsList.ExistsByName(const AName: string; AExclude : TScriptSign): boolean;
begin
  Result := FindAndGetByName(AName, AExclude, AExclude);
end;//TScriptSignsList.ExistsByName
//==============================================================================
function TScriptSignsList.Add(const AName: string; const AIsNumbered: boolean;
  const ASortOrder: integer): TScriptSign;
begin

  if ExistsByName(AName) then
    raise Exception.CreateFmt('ScriptSign с именем «%s» уже существует!', [AName]);

  Result := TScriptSign.Create(Self, AName, AIsNumbered, ASortOrder);

  inherited Add(Result);
end;//TScriptSignsList.Add
//==============================================================================
procedure TScriptSignsList.ReadFromXML(const ANode: IXMLNode);
var
  i : integer;
  vItem : TScriptSign;
begin
  for i := 0 to ANode.ChildNodes.Count - 1 do  begin
    vItem := FindByName(ANode.ChildNodes[i].Text);

    vItem.ReadFromXML(ANode.ChildNodes[i]);
  end;//for
end;//TScriptSignsList.ReadFromXML
//==============================================================================
procedure TScriptSignsList.WriteToXML(const ANode: IXMLNode);
var
  i : integer;
  vChildNode : IXMLNode;
begin
  ANode.ChildNodes.Clear;

  for i := 0 to Count - 1 do  begin
    vChildNode := ANode.AddChild('ScriptSign');
    vChildNode.Text := Items[i].Name;
    Items[i].WriteToXML(vChildNode);
  end;//for
end;//TScriptSignsList.WriteToXML
//==============================================================================

//==============================================================================
// TCyclicParam
//==============================================================================
// protected
//==============================================================================
procedure TCyclicParam.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TCyclicParam) then
    inherited

  else  with (Dest as TCyclicParam) do  begin

  end;//else
end;//TCyclicParam.AssignTo
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TCyclicParam.Create(AOwnerList: TCyclicParamsList; const AID : integer);
begin
  inherited Create;

  FOwnerList := AOwnerList;

  FID := AID;
end;//TCyclicParam.Create
//==============================================================================
destructor TCyclicParam.Destroy;
begin
  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);

  inherited;
end;//TCyclicParam.Destroy
//==============================================================================

//==============================================================================
// TCyclicParamsList
//==============================================================================
// private
//==============================================================================
function TCyclicParamsList.GetItem(Index: Integer): TCyclicParam;
begin
  Result := TCyclicParam(inherited Items[Index]);
end;//TCyclicParamsList.GetItem
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TCyclicParamsList.Extract(Item: TCyclicParam): TCyclicParam;
begin
  Result := TCyclicParam(inherited Extract(Item));

  if Assigned(Item) and (Item.FOwnerList = Self) then
    Item.FOwnerList := nil;
end;//TCyclicParamsList.Extract
//==============================================================================
function TCyclicParamsList.First: TCyclicParam;
begin
  Result := TCyclicParam(inherited First);
end;//TCyclicParamsList.First
//==============================================================================
function TCyclicParamsList.Last: TCyclicParam;
begin
  Result := TCyclicParam(inherited Last);
end;//TCyclicParamsList.Last
//==============================================================================
function TCyclicParamsList.GenID: integer;
begin
  Inc(FIDCounter);
  Result := FIDCounter;
end;//TCyclicParamsList.GenID
//==============================================================================
function TCyclicParamsList.FindAndGetByID(const AID: integer;
  var ACyclicParam: TCyclicParam; AExclude: TCyclicParam): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if  (Items[i] <> AExclude)
    and (Items[i].FID = AID)
    then  begin
      ACyclicParam := Items[i];
      Result := True;
      Exit;
    end;//if; for

  Result := False;
end;//TCyclicParamsList.FindAndGetByID
//==============================================================================
function TCyclicParamsList.FindByID(const AID: integer;
  AExclude: TCyclicParam): TCyclicParam;
begin
  Result := nil;
  FindAndGetByID(AID, Result, AExclude);
end;//TCyclicParamsList.FindByID
//==============================================================================
function TCyclicParamsList.ExistsByID(const AID: integer;
  AExclude: TCyclicParam): boolean;
begin
  Result := FindAndGetByID(AID, AExclude, AExclude);
end;//TCyclicParamsList.ExistsByID
//==============================================================================
function TCyclicParamsList.FindAndGetByText(const AText: string;
  var ACyclicParam: TCyclicParam; AExclude: TCyclicParam): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if  (Items[i] <> AExclude)
    and (Items[i].Text = AText)
    then  begin
      ACyclicParam := Items[i];
      Result := True;
      Exit;
    end;//if; for

  Result := False;
end;//TCyclicParamsList.FindAndGetByText
//==============================================================================
function TCyclicParamsList.FindByText(const AText: string;
  AExclude: TCyclicParam): TCyclicParam;
begin
  Result := nil;
  FindAndGetByText(AText, Result, AExclude);
end;//TCyclicParamsList.FindByText
//==============================================================================
function TCyclicParamsList.ExistsByText(const AText: string;
  AExclude: TCyclicParam): boolean;
begin
  Result := FindAndGetByText(AText, AExclude, AExclude);
end;//TCyclicParamsList.ExistsByText
//==============================================================================
function TCyclicParamsList.Add(const AText: string; AID : integer): TCyclicParam;
begin

  if ExistsByText(AText) then
    raise Exception.CreateFmt('Уже есть циклический параметр с именем «%s»!', [AText]);

  if AID = SCG_GEN_ID then
    AID := GenID;

  Result := TCyclicParam.Create(Self, AID);
  Result.FText := AText;
  
  inherited Add(Result);
end;//TCyclicParamsList.Add
//==============================================================================
function TCyclicParamsList.TryAdd(const AText: string; const AID : integer): TCyclicParam;
// Попытка добавить элемент
begin
  if ExistsByText(AText) then
    Result := nil
  else
    Result := Add(AText);
end;//TCyclicParamsList.TryAdd
//==============================================================================
function TCyclicParamsList.AddCyclicParams(ACyclicParams: TCyclicParamsList): integer;
// Добавить в Self все параметры из ACyclicParams, которых еще нет в Self
// Возвращает число добавленых параметров
var
  i : integer;
  vCyclicParam : TCyclicParam;
begin
  Result := 0;

  for i := 0 to ACyclicParams.Count - 1 do  begin
    vCyclicParam := TryAdd(ACyclicParams[i].Text);

    if not Assigned(vCyclicParam) then
      Continue;

    vCyclicParam.Assign(ACyclicParams[i]);

    Inc(Result);
  end;//for
end;//TCyclicParamsList.AddCyclicParams
//==============================================================================
function TCyclicParamsList.DeleteOtherCyclicParams(ACyclicParams: TCyclicParamsList): integer;
// Удалить из Self все параметры, которых нет в ACyclicParams
// Возвращает число удаленных параметров
var
  i : integer;
begin
  Result := 0;

  for i := Count - 1 downto 0 do
    if not ACyclicParams.ExistsByText(Items[i].Text) then  begin
      Delete(i);
      Inc(Result);
    end;//if; for
end;//TCyclicParamsList.DeleteOtherCyclicParams
//==============================================================================

//==============================================================================
// TConditionParam 
//==============================================================================
// private
//==============================================================================
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TConditionParam.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TConditionParam) then
    inherited

  else  with (Dest as TConditionParam) do  begin


  end;//else
end;//TConditionParam.AssignTo
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TConditionParam.Create(AOwnerList: TConditionParamsList; const AName : string);
begin
  inherited Create;

  FOwnerList := AOwnerList;

  FCyclicParamsDefs := TStringList.Create;

  FName := AName;
end;//TConditionParam.Create
//==============================================================================
destructor TConditionParam.Destroy;
begin
  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);

  FreeAndNil(FCyclicParamsDefs);

  inherited;
end;//TConditionParam.Destroy
//==============================================================================

//==============================================================================
// TConditionParamsList 
//==============================================================================
// private
//==============================================================================
function TConditionParamsList.GetItem(Index: Integer): TConditionParam;
begin
  Result := TConditionParam(inherited Items[Index]);
end;//TConditionParamsList.GetItem
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TConditionParamsList.Extract(Item: TConditionParam): TConditionParam;
begin
  Result := TConditionParam(inherited Extract(Item));

  if Assigned(Item) and (Item.FOwnerList = Self) then 
    Item.FOwnerList := nil;
end;//TConditionParamsList.Extract
//==============================================================================
function TConditionParamsList.First: TConditionParam;
begin
  Result := TConditionParam(inherited First);
end;//TConditionParamsList.First
//==============================================================================
function TConditionParamsList.Last: TConditionParam;
begin
  Result := TConditionParam(inherited Last);
end;//TConditionParamsList.Last
//==============================================================================
function TConditionParamsList.FindAndGetByName(const AName: string;
  var AConditionParam: TConditionParam; AExclude: TConditionParam): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if  (Items[i] <> AExclude)
    and (Items[i].Name = AName)
    then  begin
      AConditionParam := Items[i];
      Result := True;
      Exit;
    end;//if; for

  Result := False;
end;//TConditionParamsList.FindAndGetByName
//==============================================================================
function TConditionParamsList.FindByName(const AName: string;
  AExclude: TConditionParam): TConditionParam;
begin
  Result := nil;
  FindAndGetByName(AName, Result, AExclude);
end;//TConditionParamsList.FindByName
//==============================================================================
function TConditionParamsList.ExistsByName(const AName: string;
  AExclude: TConditionParam): boolean;
begin
  Result := FindAndGetByName(AName, AExclude, AExclude);
end;//TConditionParamsList.ExistsByName
//==============================================================================
function TConditionParamsList.Add(const AName: string): TConditionParam;
begin

  if ExistsByName(AName) then
    raise Exception.CreateFmt('Уже есть условный параметр с именем «%s»!', [AName]);

  Result := TConditionParam.Create(Self, AName);

  inherited Add(Result);
end;//TConditionParamsList.Add
//==============================================================================
function TConditionParamsList.TryAdd(const AName: string): TConditionParam;
// Попытка добавления, при неудаче возвращает nil
begin
  if ExistsByName(AName) then
    Result := nil
  else
    Result := Add(AName);
end;//TConditionParamsList.TryAdd
//==============================================================================
function TConditionParamsList.AddConditionParams(AConditionParams: TConditionParamsList): integer;
// Добавить в Self все параметры из AConditionParams, которых еще нет в Self
// Возвращает число добавленых параметров
var
  i : integer;
  vConditionParam : TConditionParam;
begin
  Result := 0;

  for i := 0 to AConditionParams.Count - 1 do  begin
    vConditionParam := TryAdd(AConditionParams[i].Name);

    if not Assigned(vConditionParam) then
      Continue;

    vConditionParam.Assign(AConditionParams[i]);

    Inc(Result);
  end;//for
end;//TConditionParamsList.AddConditionParams
//==============================================================================
function TConditionParamsList.DeleteOtherConditionParams(AConditionParams: TConditionParamsList): integer;
// Удалить из Self все параметры, которых нет в AConditionParams
// Возвращает число удаленных параметров
var
  i : integer;
begin
  Result := 0;

  for i := Count - 1 downto 0 do
    if not AConditionParams.ExistsByName(Items[i].Name) then  begin
      Delete(i);
      Inc(Result);
    end;//if; for
end;//TConditionParamsList.DeleteOtherConditionParams
//==============================================================================

//==============================================================================
// TCndByCyclValList
//==============================================================================
// private
//==============================================================================
function TCndByCyclValList.Get_Item(const Index: integer): PCndByCyclValRec;
begin
  Result := PCndByCyclValRec(inherited Items[Index]);
end;//TCndByCyclValList.Get_Item
//==============================================================================
//==============================================================================
// protected
//==============================================================================
procedure TCndByCyclValList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  inherited;

  if Action = lnDeleted then
    Dispose(Ptr);
end;//TCndByCyclValList.Notify
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TCndByCyclValList.Extract(Item: PCndByCyclValRec): PCndByCyclValRec;
begin
  Result := PCndByCyclValRec(inherited Extract(Item));
end;//TCndByCyclValList.Extract
//==============================================================================
function TCndByCyclValList.First: PCndByCyclValRec;
begin
  Result := PCndByCyclValRec(inherited First);
end;//TCndByCyclValList.First
//==============================================================================
function TCndByCyclValList.Last: PCndByCyclValRec;
begin
  Result := PCndByCyclValRec(inherited Last);
end;//TCndByCyclValList.Last
//==============================================================================
function TCndByCyclValList.FindAndGet(const ACyclicParamID: integer;
  const ACndParamName: string; var AItem: PCndByCyclValRec): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if  (Items[i].CyclicParamID  = ACyclicParamID)
    and (Items[i].CndParamName   = ACndParamName)
    then  begin
      AItem := Items[i];
      Result := True;
      Exit;
    end;//if; for

  Result := False;
end;//TCndByCyclValList.FindAndGet
//==============================================================================
function TCndByCyclValList.Find(const ACyclicParamID: integer;
  const ACndParamName: string): PCndByCyclValRec;
begin
  Result := nil;
  FindAndGet(ACyclicParamID, ACndParamName, Result);
end;//TCndByCyclValList.Find
//==============================================================================
function TCndByCyclValList.Exists(const ACyclicParamID: integer;
  const ACndParamName: string): boolean;
var
  vItem : PCndByCyclValRec;
begin
  Result := FindAndGet(ACyclicParamID, ACndParamName, vItem);
end;//TCndByCyclValList.Exists
//==============================================================================
function TCndByCyclValList.Add(const ACyclicParamID: integer;
  const ACndParamName, AValue: string): PCndByCyclValRec;
begin

  if Exists(ACyclicParamID, ACndParamName) then
    raise Exception.CreateFmt('Уже есть запись для параметров: %d «%s»!', [ACyclicParamID, ACndParamName]);


  New(Result);

  with Result^ do  begin
    CyclicParamID  := ACyclicParamID;
    CndParamName   := ACndParamName;
    Value          := AValue;
  end;//with

  inherited Add(Result);
end;//TCndByCyclValList.Add
//==============================================================================

//==============================================================================
// TConstParam
//==============================================================================
// protected
//==============================================================================
procedure TConstParam.AssignTo(Dest: TPersistent);
begin
  if not (Dest is TConstParam) then
    inherited

  else  with (Dest as TConstParam) do  begin
    Text := Self.Text;

  end;//else
end;//TConstParam.AssignTo
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TConstParam.Create(AOwnerList: TConstParamsList; const AName: string);
begin
  inherited Create;

  FOwnerList := AOwnerList;

  FName := AName;
end;//TConstParam.Create
//==============================================================================
destructor TConstParam.Destroy;
begin
  if Assigned(FOwnerList) then
    FOwnerList.Extract(Self);

  inherited;
end;//TConstParam.Destroy
//==============================================================================

//==============================================================================
// TConstParamsList
//==============================================================================
function TConstParamsList.GetItem(Index: Integer): TConstParam;
begin
  Result := TConstParam(inherited Items[Index]);
end;//TConstParamsList.GetItem
//==============================================================================
//==============================================================================
// public
//==============================================================================
function TConstParamsList.Extract(Item: TConstParam): TConstParam;
begin
  Result := TConstParam(inherited Extract(Item));
end;//TConstParamsList.Extract
//==============================================================================
function TConstParamsList.First: TConstParam;
begin
  Result := TConstParam(inherited First);
end;//TConstParamsList.First
//==============================================================================
function TConstParamsList.Last: TConstParam;
begin
  Result := TConstParam(inherited Last);
end;//TConstParamsList.Last
//==============================================================================
function TConstParamsList.FindAndGetByName(const AName: string; var AConstParam: TConstParam;
  AExclude: TConstParam): boolean;
var
  i : integer;
begin
  for i := 0 to Count - 1 do
    if  (Items[i] <> AExclude)
    and (Items[i].Name = AName)
    then  begin
      AConstParam := Items[i];
      Result := True;
      Exit;
    end;//if; for

  Result := False;
end;//TConstParamsList.FindAndGetByName
//==============================================================================
function TConstParamsList.FindByName(const AName: string; AExclude: TConstParam): TConstParam;
begin
  Result := nil;
  FindAndGetByName(AName, Result, AExclude);
end;//TConstParamsList.FindByName
//==============================================================================
function TConstParamsList.ExistsByName(const AName: string; AExclude: TConstParam): boolean;
begin
  Result := FindAndGetByName(AName, AExclude, AExclude);
end;//TConstParamsList.ExistsByName
//==============================================================================
function TConstParamsList.GenUniqueName: string;
var
  i : integer;
begin
  i := 0;
  repeat
    Inc(i);

    Result := Format('<Const%d>', [i]);
  until not ExistsByName(Result);
end;//TConstParamsList.GenUniqueName
//==============================================================================
function TConstParamsList.Add(const AName: string): TConstParam;
begin
  if ExistsByName(AName) then
    raise Exception.CreateFmt('Уже есть константа с именем «%s»!', [AName]);

  Result := TConstParam.Create(Self, AName);

  inherited Add(Result);
end;//TConstParamsList.Add
//==============================================================================
function TConstParamsList.TryAdd(const AName: string): TConstParam;
// Попытка добавления, при неудаче возвращает nil
begin
  if ExistsByName(AName) then
    Result := nil
  else
    Result := Add(AName);
end;//TConstParamsList.TryAdd
//==============================================================================
function TConstParamsList.AddConstParams(AConstParams: TConstParamsList): integer;
// Добавить в Self все параметры из AConstParams, которых еще нет в Self
// Возвращает число добавленых параметров
var
  i : integer;
  vConstParam : TConstParam;
begin
  Result := 0;

  for i := 0 to AConstParams.Count - 1 do  begin
    vConstParam := TryAdd(AConstParams[i].Name);

    if not Assigned(vConstParam) then
      Continue;

    vConstParam.Assign(AConstParams[i]);

    Inc(Result);
  end;//for
end;//TConstParamsList.AddConstParams
//==============================================================================
function TConstParamsList.DeleteOtherConstParams(AConstParams: TConstParamsList): integer;
// Удалить из Self все параметры, которых нет в AConstParams
// Возвращает число удаленных параметров
var
  i : integer;
begin
  Result := 0;

  for i := Count - 1 downto 0 do
    if (Items[i].ParamKind = pkAutoGen) and (not AConstParams.ExistsByName(Items[i].Name)) then  begin
      Delete(i);
      Inc(Result);
    end;//if; for
end;//TConstParamsList.DeleteOtherConstParams
//==============================================================================

//==============================================================================
// TParamsComplex
//==============================================================================
// private
//==============================================================================
function TParamsComplex.Get_CndByCyclVals(const ACyclicParamID: integer;
  const ACndParamName: string): string;
var
  vCndByCyclVal : PCndByCyclValRec;
begin
  if FCndByCyclVals.FindAndGet(ACyclicParamID, ACndParamName, vCndByCyclVal) then
    Result := vCndByCyclVal.Value

  else
    Result := '';
end;//TParamsComplex.Get_CndByCyclVals
//==============================================================================
procedure TParamsComplex.Set_CndByCyclVals(const ACyclicParamID: integer;
  const ACndParamName, Value: string);
var
  vCndByCyclVal : PCndByCyclValRec;
begin
  if FCndByCyclVals.FindAndGet(ACyclicParamID, ACndParamName, vCndByCyclVal) then
    vCndByCyclVal^.Value := Value
  else
    FCndByCyclVals.Add(ACyclicParamID, ACndParamName, Value);
end;//TParamsComplex.Set_CndByCyclVals
//==============================================================================
//==============================================================================
// public
//==============================================================================
constructor TParamsComplex.Create;
begin
  inherited Create;

  FCyclicParams     := TCyclicParamsList.Create;
  FConditionParams  := TConditionParamsList.Create;
  FCndByCyclVals    := TCndByCyclValList.Create;
  FConstants        := TConstParamsList.Create;

end;//TParamsComplex.Create
//==============================================================================
destructor TParamsComplex.Destroy;
begin
  FreeAndNil(FCyclicParams);   
  FreeAndNil(FConditionParams);
  FreeAndNil(FCndByCyclVals);
  FreeAndNil(FConstants);

  inherited;
end;//TParamsComplex.Destroy
//==============================================================================
procedure TParamsComplex.Clear;
begin
  FCyclicParams.Clear;
  FConditionParams.Clear;
  FConstants.Clear;
end;//TParamsComplex.Clear
//==============================================================================
procedure TParamsComplex.RegisterParam(const ASign: TScriptSign; const AParamName : string);
begin
  if ASign.Name = SCG_CONDITION_PARAM_SIGN then
    ConditionParams.TryAdd(AParamName)

  else  if ASign.Name = SCG_CONST_PARAM_SIGN then
    Constants.TryAdd(AParamName)

  else
    // наверное ничего не делать ???
end;//TParamsComplex.RegisterParam
//==============================================================================
function TParamsComplex.AddParams(AParamsComplex: TParamsComplex) : integer;
// Добавить в Self все параметры, содержащиеся в AParamsComplex, если их нет
begin
  Result :=
    CyclicParams.AddCyclicParams(AParamsComplex.CyclicParams) +
    ConditionParams.AddConditionParams(AParamsComplex.ConditionParams) +
    Constants.AddConstParams(AParamsComplex.Constants);
end;//TParamsComplex.AddParams
//==============================================================================
function TParamsComplex.DeleteOtherParams(AParamsComplex: TParamsComplex;
  const ACyclicParamsFlag : boolean) : integer;
// Удалить из Self все параметры, которых нет в AParamsComplex
begin
  Result :=
    ConditionParams.DeleteOtherConditionParams(AParamsComplex.ConditionParams) +
    Constants.DeleteOtherConstParams(AParamsComplex.Constants);

  if ACyclicParamsFlag then
    Result := Result + CyclicParams.DeleteOtherCyclicParams(AParamsComplex.CyclicParams);

end;//TParamsComplex.DeleteOtherParams
//==============================================================================
procedure TParamsComplex.ClearGarbage;
// Убрать соответствия для параметров, которые уже не существуют
var
  i : integer;
begin
  for i := FCndByCyclVals.Count - 1 downto 0 do
    with FCndByCyclVals[i]^ do
      if not (
        CyclicParams.ExistsByID(CyclicParamID)
        and
        ConditionParams.ExistsByName(CndParamName)
        )
      then
        FCndByCyclVals.Delete(i);
end;//TParamsComplex.ClearGarbage
//==============================================================================

//==============================================================================
// TCGDM
//==============================================================================
// private
//==============================================================================
function TCGDM.Get_InstanceHandle: THandle;
begin
  Result := GetModuleHandle('SANCodeGeneratorPKG.bpl')
end;//TCGDM.Get_InstanceHandle
//==============================================================================
procedure TCGDM.Set_RememberTemplateCodes(const Value: boolean);
var
  i : integer;
  vRTCNode : IXMLNode;
begin
  if FRememberTemplateCodes = Value then
    Exit;


  FRememberTemplateCodes := Value;


  vRTCNode := FXMLNode.ChildNodes.FindNode('RememberTemplateCodes');
  if not Assigned(vRTCNode) then
    vRTCNode := FXMLNode.AddChild('RememberTemplateCodes');

  vRTCNode.ChildNodes.Clear;
  if FRememberTemplateCodes then
    for i := 0 to TemplateCodes.Count - 1 do
      if FileExists(TemplateCodes[i].TemplateName) then
        vRTCNode.AddChild('TemplateCode').Text := TemplateCodes[i].TemplateName;

  xmlConfig.SaveToFile;
end;//TCGDM.Set_RememberTemplateCodes
//==============================================================================
function TCGDM.Get_CyclicParamSign: TScriptSign;
begin
  Result := ScriptSigns.FindByName(SCG_CYCLIC_PARAM_SIGN);
end;//TCGDM.Get_CyclicParamSign
//==============================================================================
function TCGDM.Get_ConditionParamSign: TScriptSign;
begin
  Result := ScriptSigns.FindByName(SCG_CONDITION_PARAM_SIGN);
end;//TCGDM.Get_ConditionParamSign
//==============================================================================
function TCGDM.Get_ConstParamSign: TScriptSign;
begin
  Result := ScriptSigns.FindByName(SCG_CONST_PARAM_SIGN);
end;//TCGDM.Get_ConstParamSign
//==============================================================================
function TCGDM.Get_SpacerSign: TScriptSign;
begin
  Result := ScriptSigns.FindByName(SCG_SPACER_SIGN);
end;//TCGDM.Get_SpacerSign
//==============================================================================
procedure TCGDM.Set_ParamsTextSourceAutoParseMode(const Value: boolean);
begin
  if FParamsTextSourceAutoParseMode = Value then
    Exit;

  FParamsTextSourceAutoParseMode := Value;

  if FParamsTextSourceAutoParseMode then
    ParseParamsTextSource;

  SetXMLBoolAttr(FParamsNode, 'ParamsTextSourceAutoParseMode', Value);
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamsTextSourceAutoParseMode
//==============================================================================
procedure TCGDM.Set_ParamTextSourceSeporationKind(const Value: TParamTextSourceSeporationKind);
begin
  if FParamTextSourceSeporationKind = Value then
    Exit;

  FParamTextSourceSeporationKind := Value;

  ParseParamsTextSource;

  FParamsNode.Attributes['ParamTextSourceSeporationKind'] := Ord(Value);
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamTextSourceSeporationKind
//==============================================================================
procedure TCGDM.Set_ParamsDeleteNonExistsCyclic(const Value: boolean);
begin
  if FParamsDeleteNonExistsCyclic = Value then
    Exit;

  FParamsDeleteNonExistsCyclic := Value;

  SetXMLBoolAttr(FParamsNode, 'ParamsDeleteNonExistsCyclic', Value);
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamsDeleteNonExistsCyclic
//==============================================================================
procedure TCGDM.Set_ParamsDeleteNonExistsCondition(const Value: boolean);
begin
  if FParamsDeleteNonExistsCondition = Value then
    Exit;

  FParamsDeleteNonExistsCondition := Value;

  SetXMLBoolAttr(FParamsNode, 'ParamsDeleteNonExistsCondition', Value);
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamsDeleteNonExistsCondition
//==============================================================================
procedure TCGDM.Set_ParamsDeleteNonExistsConsts(const Value: boolean);
begin
  if FParamsDeleteNonExistsConsts = Value then
    Exit;

  FParamsDeleteNonExistsConsts := Value;

  SetXMLBoolAttr(FParamsNode, 'ParamsDeleteNonExistsConsts', Value);
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamsDeleteNonExistsConsts
//==============================================================================
procedure TCGDM.Set_ParamTextSourceSepSubStr(const Value: string);
begin
  if FParamTextSourceSepSubStr = Value then
    Exit;


  FParamTextSourceSepSubStr := Value;


  if FParamTextSourceSeporationKind = ptsskSubString then
    ParseParamsTextSource;


  FParamsNode.Attributes['ParamTextSourceSepSubStr'] := Value;
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamTextSourceSepSubStr
//==============================================================================
procedure TCGDM.Set_ParamTextSourceSepWordsEndChars(const Value: string);
begin
  if FParamTextSourceSepWordsEndChars = Value then
    Exit;


  FParamTextSourceSepWordsEndChars := Value;


  if FParamTextSourceSeporationKind = ptsskWord then
    ParseParamsTextSource;


  FParamsNode.Attributes['ParamTextSourceSepWordsEndChars'] := Value;
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamTextSourceSepWordsEndChars
//==============================================================================
procedure TCGDM.Set_ParamsTrimAll(const Value: boolean);
begin
  if FParamsTrimAll = Value then
    Exit;


  FParamsTrimAll := Value;


  ParseParamsTextSource;


  SetXMLBoolAttr(FParamsNode, 'ParamsTrimAll', Value);
  xmlConfig.SaveToFile;
end;//TCGDM.Set_ParamsTrimAll
//==============================================================================
//==============================================================================
// protected
//==============================================================================
// IIDEEXT_MainToolsDLG
//==============================================================================
function TCGDM.ShowMainToolsDLG: boolean;
begin
  Result := CallToolsDLG;
end;//TCGDM.ShowMainToolsDLG
//==============================================================================
// ICG_Controller
//==============================================================================
function TCGDM.Get_OnInsertResult: TNotifyEvent;
begin
  Result := FOnInsertResult;
end;//TCGDM.Get_OnInsertResult
//==============================================================================
procedure TCGDM.Set_OnInsertResult(const AOnInsertResult: TNotifyEvent);
begin
  FOnInsertResult := AOnInsertResult;
end;//TCGDM.Set_OnInsertResult
//==============================================================================
procedure TCGDM.Show;
begin
  MainForm.Show;
end;//TCGDM.Show
//==============================================================================
//==============================================================================
function TCGDM.FindUnLinkedTemplateFrame(var AFrame: TFrame): boolean;
// Поиск освободившегося фрейма
var
  i : integer;
begin
  for i := 0 to ComponentCount - 1 do
    if  (Components[i] is TframeTemplateCode)
    and (TframeTemplateCode(Components[i]).TemplateCode = nil)
    then  begin
      AFrame := TframeTemplateCode(Components[i]);
      Break;
    end;//if; for

 Result := Assigned(AFrame);
end;//TCGDM.FindUnLinkedTemplateFrame
//==============================================================================
function TCGDM.CreateNewTemplateFrame: TFrame;
begin
  Inc(FFramesUniqueGen);
  Result := TframeTemplateCode.Create(Self);
  Result.Name := Format('frameTemplateCode%d', [FFramesUniqueGen]);
  Result.Tag := FFramesUniqueGen;
end;//TCGDM.CreateNewTemplateFrame
//==============================================================================
function TCGDM.GetXMLBoolAttr(ANode: IXMLNode; const AAttrName: string;
  const ADefault: boolean): boolean;
var
  S : string;
begin
  Result := ADefault;

  if ANode.Attributes[AAttrName] = Null then
    Exit;

  S := ANode.Attributes[AAttrName];

  if S = BoolToStr(True, True) then
    Result := True
  else  if S = BoolToStr(False, True) then
    Result := False;
end;//TCGDM.GetXMLBoolAttr
//==============================================================================
procedure TCGDM.SetXMLBoolAttr(ANode: IXMLNode; const AAttrName: string; const AValue: boolean);
begin
  ANode.Attributes[AAttrName] := BoolToStr(AValue, True);
end;//TCGDM.SetXMLBoolAttr
//==============================================================================
//==============================================================================
// public
//==============================================================================
class function TCGDM.AppDataFolder(const AForceDirectories: boolean): string;
begin
  Result := GetSpecialFolderPath(CSIDL_COMMON_APPDATA) + '\' + SAN_CODE_GENERATOR_FOLDER_PATH;

  if AForceDirectories then
    ForceDirectories(Result);
end;//TCGDM.AppDataFolder
//==============================================================================
class function TCGDM.AppDataTemplatesFolder(const AForceDirectories: boolean): string;
begin
  Result := AppDataFolder(False) + '\TemplateCodes';

  if AForceDirectories then
    ForceDirectories(Result);
end;//function TCGDM.AppDataTemplatesFolder
//==============================================================================
procedure TCGDM.CreateSplash;
begin
  FSplashForm := TfrmCGSplash.Create(nil);
  FSplashForm.Show;
  FSplashForm.Repaint;
end;//TCGDM.CreateSplash
//==============================================================================
procedure TCGDM.DestroySplash;
begin
  FreeAndNil(FSplashForm);
end;//TCGDM.DestroySplash
//==============================================================================
procedure TCGDM.ReCreateMainForm(const AShowNow : boolean);
// Имеет смысл только для подключаемого редактора
var
  i : integer;
begin
  if Assigned(FMainForm) then  begin

    for i := 0 to TemplateCodes.Count - 1 do
      TemplateCodes[i].Frame.Parent := nil;

    FreeAndNil(FMainForm);
  end;//if


  if FRunModeAsApp then
    Application.CreateForm(TfrmCGMain, FMainForm)
  else
    FMainForm := TfrmCGMain.Create(Self);


  TfrmCGMainHack(FMainForm).SetCGDM(Self);


  with TfrmCGMain(FMainForm) do
    if FRunModeAsApp then
      begin
        BorderStyle := bsSizeable;

      end
    else
      begin
        BorderStyle := bsSizeToolWin;

      end;//else if; with TfrmCGMain


  if AShowNow then
    FMainForm.Show;
end;//TCGDM.ReCreateMainForm
//==============================================================================
function TCGDM.OpenTemplateCode(var ATemplateCode: TTemplateCode): integer;
// Загрузить все шаблоны, открытые диалогом
var
  i : integer;
begin
  Result := 0;
  with dlgOpenTemplate do  begin

    if not Execute then
      Exit;

    for i := 0 to Files.Count - 1 do  begin
      ATemplateCode := TemplateCodes.Find(Files[i]);

      if not Assigned(ATemplateCode) then
        ATemplateCode := TemplateCodes.Add(Files[i]);

      ATemplateCode.LoadCode;
    end;//for

    Result := Files.Count;
  end;//with
end;//TCGDM.OpenTemplateCode
//==============================================================================
function TCGDM.SaveTemplateCode(const ATemplateCode: TTemplateCode): boolean;
begin

  if not Assigned(ATemplateCode) then  begin
    Result := False;
    Exit;
  end;//if


  with dlgSaveTemplate do  begin
    FileName := ATemplateCode.TemplateName;

    Result := Execute;
    if not Result then
      Exit;

    ATemplateCode.FTemplateName := FileName;
    ATemplateCode.SaveCode;
  end;//with
end;//TCGDM.SaveTemplateCode
//==============================================================================
function TCGDM.SaveResult(const AResultStrings: TStrings): boolean;
begin
  if not Assigned(AResultStrings) then  begin
    Result := False;
    Exit;
  end;//if


  with dlgSaveTemplate do  begin

    Result := Execute;
    if not Result then
      Exit;

    AResultStrings.SaveToFile(FileName);
  end;//with
end;//TCGDM.SaveResult
//==============================================================================
function TCGDM.CallToolsDLG: boolean;
var
  vSSNode : IXMLNode;
begin

  // Диалог настроек
  with TfrmToolsDLG.Create(nil) do  try
    Caption := 'Настройки';

    fSSCyclicParam.Sign.Assign(Self.CyclicParamSign);
    fSSConditionParam.Sign.Assign(Self.ConditionParamSign);
    fSSConstParam.Sign.Assign(Self.ConstParamSign);
    fSSSpacer.Sign.Assign(Self.SpacerSign);


    Result := (ShowModal = mrOK);
    if not Result then
      Exit;


    Self.CyclicParamSign.Assign(fSSCyclicParam.Sign);
    Self.ConditionParamSign.Assign(fSSConditionParam.Sign);
    Self.ConstParamSign.Assign(fSSConstParam.Sign);
    Self.SpacerSign.Assign(fSSSpacer.Sign);

  finally
    Free;
  end;//t..f


  // Сохранение настроек
  vSSNode := FXMLNode.ChildNodes.FindNode('ScriptSigns');
  if not Assigned(vSSNode) then
    vSSNode := FXMLNode.AddChild('ScriptSigns');

  ScriptSigns.WriteToXML(vSSNode);

  xmlConfig.SaveToFile;
end;//TCGDM.CallToolsDLG
//==============================================================================
procedure TCGDM.ParamsAddFromTemplateCode(const ATemplateCode: TTemplateCode);
// Добавление параметров
var
  vTmpPC : TParamsComplex;
begin
  vTmpPC := TframeTemplateCode(ATemplateCode.Frame).ParamsComplex;

  ParamsComplex.AddParams(vTmpPC);

  if ParamsDeleteNonExistsCondition then
    ParamsComplex.ConditionParams.DeleteOtherConditionParams(vTmpPC.ConditionParams);

  if ParamsDeleteNonExistsConsts then
    ParamsComplex.Constants.DeleteOtherConstParams(vTmpPC.Constants);

//  ParamsComplex.ClearGarbage;
end;//TCGDM.ParamsAddFromTemplateCode
//==============================================================================
procedure TCGDM.ParseParamsTextSource;
// По текстовому источнику получить список параметров
//------------------------------------------------------------------------------
  procedure TryAddBayStrings(const AStrings : TStrings);
  // Добавить циклические параметры из списка строк
  var
    i : integer;
    S : string;
  begin
    for i := 0 to AStrings.Count - 1 do  begin
      S := AStrings[i];

      if ParamsTrimAll then
        S := Trim(S);

      ParamsComplex.CyclicParams.TryAdd(S);
    end;//for
  end;//TryAddBayStrings
//------------------------------------------------------------------------------
var
  vPTSLines, vStrings : TStrings;
  vCursor : TCursor;
begin

  if ParamsDeleteNonExistsCyclic then
    ParamsComplex.CyclicParams.Clear;


  // Здесь прямо используется главная форма
  if not Assigned(FMainForm) then
    Exit;


  vCursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;

    vPTSLines := TfrmCGMain(FMainForm).mmParamsTextSource.Lines;


    case ParamTextSourceSeporationKind of
      ptsskLine:
        TryAddBayStrings(vPTSLines);

      ptsskWord, ptsskSubString:  begin
        vStrings := TStringList.Create;
        try
          if ParamTextSourceSeporationKind = ptsskWord then
            String_ExtractWords(StringReplace(vPTSLines.Text, #13#10, ' ', [rfReplaceAll]), vStrings, ParamTextSourceSepWordsEndChars)
          else
            vStrings.Text := StringReplace(StringReplace(vPTSLines.Text, #13#10, '', [rfReplaceAll]), ParamTextSourceSepSubStr, #13#10, [rfReplaceAll]);

          TryAddBayStrings(vStrings);

        finally
          FreeAndNil(vStrings);
        end;//t..f
      end;//ptsskWord, ptsskSubString

    end;//case

    TfrmCGMainHack(FMainForm).fCyclParams.UpdateGrid;
    TfrmCGMainHack(FMainForm).fCndParamsDefs.UpdateList;
  finally
    Screen.Cursor := vCursor;
  end;//t..f
end;//TCGDM.ParseParamsTextSource
//==============================================================================
procedure TCGDM.GenerateFor(const ATemplateCode: TTemplateCode; const AResultStrings : TStrings);
// Сгенерировать результаты для шаблона ATemplateCode
//------------------------------------------------------------------------------
  procedure Spacers_ShowMessage(const AFragmentBeg, AFragmentEnd : string;
    AStrings: TStrings; var ALine, ACol, AOffset : integer; const AFragmentName: string;
    var AComplete : boolean; AParam: Pointer);
  // Тестовая функция, показывающая имена выравнивающих пробелов
  begin
    ShowMessageFmt('Line: %d; Col: %d; SpacerName: %s', [ALine, ACol, AFragmentName]);
  end;//Spacers_ShowMessage
//------------------------------------------------------------------------------
  procedure Spacers_MAXCol(const AFragmentBeg, AFragmentEnd : string;
    AStrings: TStrings; var ALine, ACol, AOffset : integer; const AFragmentName: string;
    var AComplete : boolean; AParam: PInteger);
  // Для всех выравнивающих пробелов высчитать максимальный ABegCol
  // AParam - указатель на текущий максимум
  begin
    AParam^ := MAX(AParam^, ACol);
  end;//Spacers_MAXCol
//------------------------------------------------------------------------------
  procedure Spacers_InsertSpaces(const AFragmentBeg, AFragmentEnd : string;
    AStrings: TStrings; var ALine, ACol, AOffset : integer; const AFragmentName: string;
    var AComplete : boolean; AParam: PInteger);
  // Для всех выравнивающих пробелов втавить PInteger(AParam)^ - ABegCol пробелов
  var
    S : string;
  begin
    if ACol = AParam^ then
      Exit;

    S := AStrings[ALine];

    Insert(DupeString(' ', AParam^ - ACol), S, ACol);

    AStrings[ALine] := S;

    ACol := AParam^;
  end;//Spacers_InsertSpaces
//------------------------------------------------------------------------------
  procedure Spacers_ByName(const AFragmentBeg, AFragmentEnd : string;
    AStrings: TStrings; var ALine, ACol, AOffset : integer; const AFragmentName: string;
    var AComplete : boolean; AParam: Pointer);
  // Для всех выравнивающих пробелов провести поименную обработку
  var
    vMaxCol : integer;
  begin
    vMaxCol := ACol;

    // Высчитать максимальный отступ, причем выйти из процедуры, если нет других
    if EnumStrFragments(AFragmentBeg, AFragmentEnd, AStrings, @Spacers_MAXCol, @vMaxCol, ESF_USE_FILTER_NAME, ALine, ACol, AFragmentName) = 0 then
      Exit;

    // Вставить нужное число пробелов
    EnumStrFragments(AFragmentBeg, AFragmentEnd, AStrings, @Spacers_InsertSpaces, @vMaxCol, ESF_USE_FILTER_NAME, ALine, ACol, AFragmentName);

    // Убрать все спейсоры с именем ASpacerName и текста ASirings
    AStrings.Text := StringReplace(AStrings.Text, AFragmentBeg + AFragmentName + AFragmentEnd, '', [rfReplaceAll]);

    // Т.к. фрагмент удален, надо отменить смещение
    AOffset := 0;
//    ACol := MAX(1, ACol - (Length(AFragmentBeg) + Length(AFragmentName) + Length(AFragmentEnd) + 1));
  end;//Spacers_ByName
//------------------------------------------------------------------------------
var
  vCursor : TCursor;
  vStepResult : TStringList;
  p, c : integer;
  vCyclicParam : TCyclicParam;
begin
  vCursor := Screen.Cursor;
  try

    AResultStrings.BeginUpdate;
    try

      vStepResult := TStringList.Create;
      try

        for p := 0 to ParamsComplex.CyclicParams.Count - 1 do  begin

          // Текущий параметр
          vCyclicParam := ParamsComplex.CyclicParams[p];

          // Получение текста из шаблона
          vStepResult.Assign(ATemplateCode.TemplateText);

          // Замена циклического параметра
          vStepResult.Text := StringReplace(
            vStepResult.Text,
            CyclicParamSign.SignBeg,
            vCyclicParam.Text,
            [rfReplaceAll]
          );

          // Замена условных параметров
          for c := 0 to ParamsComplex.ConditionParams.Count - 1 do
            vStepResult.Text := StringReplace(
              vStepResult.Text,
              ConditionParamSign.SignBeg + ParamsComplex.ConditionParams[c].Name + ConditionParamSign.SignEnd,
              ParamsComplex.CndByCyclVals[vCyclicParam.ID, ParamsComplex.ConditionParams[c].Name],
              [rfReplaceAll]
            );

          // Замена констант
          for c := 0 to ParamsComplex.Constants.Count - 1 do
            vStepResult.Text := StringReplace(
              vStepResult.Text,
              ConstParamSign.SignBeg + ParamsComplex.Constants[c].Name + ConstParamSign.SignEnd,
              ParamsComplex.Constants[c].Text,
              [rfReplaceAll]
            );

          AResultStrings.AddStrings(vStepResult);
        end;//for

      finally
        FreeAndNil(vStepResult);
      end;//t..f


      // Выравнивающие пробелы
      try
          //SpacersShowMessage
//        ShowMessage(IntToStr(EnumSpacers(ATemplateCode.TemplateResult, nil, nil)));

//        EnumStrFragments(SpacerSign.SignBeg, SpacerSign.SignEnd, ATemplateCode.TemplateResult, @Spacers_ShowMessage, nil);
        EnumStrFragments(SpacerSign.SignBeg, SpacerSign.SignEnd, AResultStrings, @Spacers_ByName, nil, ESF_NOT_DUPLICATES_IN_LINE);


      except
        on E:ECGCodeSource do
          begin
            Application.MessageBox(
              PChar(E.Message),
              'Ошибка в результирующего кода',
              MB_ICONWARNING
            );

            Exit;
          end
        else
          raise;
      end;//t..e

    finally
      AResultStrings.EndUpdate;
    end;//t..f

  finally
    Screen.Cursor := vCursor;
  end;//t..f
end;//TCGDM.GenerateFor
//==============================================================================
//==============================================================================
// published
//==============================================================================
procedure TCGDM.DataModuleCreate(Sender: TObject);
var
  i : integer;
  vXMLFileName : string;
  vRTCNode, vSSNode : IXMLNode;
begin
//  CreateSplash;
  try

    FTemplateCodeList := TTemplateCodeList.Create;
    FTemplateCodeList.FCGDM := Self;


    FScriptSigns := TScriptSignsList.Create;
    with ScriptSigns do  begin
      Add(SCG_CYCLIC_PARAM_SIGN,     False,  1);
      Add(SCG_CONDITION_PARAM_SIGN,  True,   2);
      Add(SCG_CONST_PARAM_SIGN,      True,   3);
      Add(SCG_SPACER_SIGN,           True,   4);
    end;//with


    FParamsComplex := TParamsComplex.Create;


    dlgOpenTemplate.InitialDir := AppDataTemplatesFolder(True);
    dlgSaveTemplate.InitialDir := AppDataTemplatesFolder;


    vXMLFileName := AppDataFolder(True) + '\' + 'Config.xml';
    xmlConfig.Active := True;
    if not FileExists(vXMLFileName) then  begin
      xmlConfig.AddChild('XML');
      xmlConfig.SaveToFile(vXMLFileName);
    end;//if
    xmlConfig.FileName := vXMLFileName;
    xmlConfig.LoadFromFile;

    FXMLNode := xmlConfig.ChildNodes.FindNode('XML');
    if not Assigned(FXMLNode) then
      FXMLNode := xmlConfig.AddChild('XML');

    FParamsNode := FXMLNode.ChildNodes.FindNode('Params');
    if not Assigned(FParamsNode) then
      FParamsNode := FXMLNode.AddChild('Params');


    FParamsTextSourceAutoParseMode   := GetXMLBoolAttr(FParamsNode, 'ParamsTextSourceAutoParseMode',   True);
    FParamsDeleteNonExistsCyclic     := GetXMLBoolAttr(FParamsNode, 'ParamsDeleteNonExistsCyclic',     True);
    FParamsDeleteNonExistsCondition  := GetXMLBoolAttr(FParamsNode, 'ParamsDeleteNonExistsCondition',  True);
    FParamsDeleteNonExistsConsts     := GetXMLBoolAttr(FParamsNode, 'ParamsDeleteNonExistsConsts',     False);
    FParamsTrimAll                   := GetXMLBoolAttr(FParamsNode, 'ParamsTrimAll',                   True);


    if FParamsNode.Attributes['ParamTextSourceSeporationKind'] = Null then
      FParamTextSourceSeporationKind := ptsskSubString
    else
      FParamTextSourceSeporationKind := TParamTextSourceSeporationKind(FParamsNode.Attributes['ParamTextSourceSeporationKind']);

    if FParamsNode.Attributes['ParamTextSourceSepSubStr'] = Null then
      FParamTextSourceSepSubStr := ','
    else
      FParamTextSourceSepSubStr := FParamsNode.Attributes['ParamTextSourceSepSubStr'];

    if FParamsNode.Attributes['ParamTextSourceSepWordsEndChars'] = Null then
      FParamTextSourceSepWordsEndChars := ' .,;:'
    else
      FParamTextSourceSepWordsEndChars := FParamsNode.Attributes['ParamTextSourceSepWordsEndChars'];


    vRTCNode := FXMLNode.ChildNodes.FindNode('RememberTemplateCodes');
    FRememberTemplateCodes := Assigned(vRTCNode) and (vRTCNode.ChildNodes.Count > 0);
    if FRememberTemplateCodes then
      for i := 0 to vRTCNode.ChildNodes.Count - 1 do
        TemplateCodes.Add(vRTCNode.ChildNodes[i].Text).LoadCode;


    vSSNode := FXMLNode.ChildNodes.FindNode('ScriptSigns');
    if Assigned(vSSNode) then
      ScriptSigns.ReadFromXML(vSSNode);

  finally
//    DestroySplash;
  end;//t..f
end;//TCGDM.DataModuleCreate
//==============================================================================
procedure TCGDM.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FTemplateCodeList);

  FreeAndNil(FScriptSigns);

  FreeAndNil(FParamsComplex);
end;//TCGDM.DataModuleDestroy
//==============================================================================




END.
